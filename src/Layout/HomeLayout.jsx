import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";
import React from "react";

export default function HomeLayout({ contentPage }) {
  return (
    <div className="relative">
      <Header />
      {contentPage}
      <Footer />
    </div>
  );
}
