import React from 'react'
import HeaderAdmin from '../Components/HeaderAdmin/HeaderAdmin'

export default function AdminLayout({contentPage }) {
  return (
      <div>
          <HeaderAdmin />
          <div >    
          {contentPage}
          </div>
    </div>
  )
}
