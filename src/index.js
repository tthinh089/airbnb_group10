import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./redux/userSlice";
import headerSlice from "./redux/headerSlice";
import { Provider } from "react-redux";
import listUserSlice from "./redux/listUserSlice";
import listLocationSlice from "./redux/listLocationSlice";
import roomManagementSlice from "./redux/roomManagementSlice";
import bookingManagementSlice from "./redux/bookingManagementSlice";
const store = configureStore({
  reducer: {
    userSlice: userSlice,
    listUserSlice: listUserSlice,
    listLocationSlice: listLocationSlice,
    roomManagementSlice: roomManagementSlice,
    bookingManagementSlice: bookingManagementSlice,
    headerSlice: headerSlice,
  },
});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

reportWebVitals();
