import { Button, Modal, Typography, message } from "antd";
import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { https } from "../../Services/config";
import Header from "../../Components/Header/Header";
import Maps from "../../Components/Maps/Maps";
import Footer from "../../Components/Footer/Footer";
import { useSelector } from "react-redux";

export default function DanhSachPhongTablet() {
  const navigate = useNavigate();
  const  user  = useSelector((state) => state.userSlice.userInfo);
  const [open, setOpen] = useState(false);
  const { Text, Title } = Typography;
  let { cityID } = useParams();
  let [roomList, setRoomList] = useState([]);
  useEffect(() => {
    https
      .get(`/api/phong-thue/lay-phong-theo-vi-tri?maViTri=${cityID}`)
      .then((res) => {
        setRoomList(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
        message.error("Lấy thông tin thất bại!");
      });
  }, [cityID]);

  const handleNavLinkClick = (item) => {
    if (!user) {
      setOpen(true);
    } else {
      navigate(`/diaDiem/${cityID}/${item.id}`);
    }
  };

  const handleCancel = () => {
    setOpen(false);
  };

  return (
    <div className="pt-24">
      <Header />
      <div className="container mx-auto grid grid-cols-2 justify-between gap-3 pt-24">
        <div>
          <Title level={2}>Chỗ ở tại khu vực bạn đã chọn</Title>
          <Text type="secondary">Đã tìm thấy {roomList.length} chỗ ở</Text>
          <div className="space-y-4 grid-cols-1">
            {roomList.map((item, index) => {
              const trueAttributes = [];
              if (item.banLa) {
                trueAttributes.push("Bàn là");
              }
              if (item.banUi) {
                trueAttributes.push("Bàn ủi");
              }
              if (item.bep) {
                trueAttributes.push("Bếp");
              }
              if (item.dieuHoa) {
                trueAttributes.push("Điều hòa");
              }
              if (item.doXe) {
                trueAttributes.push("Đỗ xe");
              }
              if (item.hoBoi) {
                trueAttributes.push("Hồ bơi");
              }
              if (item.mayGiat) {
                trueAttributes.push("Máy giặt");
              }
              if (item.tivi) {
                trueAttributes.push("Ti vi");
              }
              if (item.wifi) {
                trueAttributes.push("WiFi");
              }

              return (
                <div
                  key={index}
                  className="flex gap-2 shadow-md rounded-md p-2"
                  onClick={() => handleNavLinkClick(item)}
                  style={{ cursor: "pointer" }}
                >
                  <img
                    src={item.hinhAnh}
                    className="w-56 h-28 rounded-xl"
                    alt="hinhAnh"
                  />
                  <div className="truncate w-fit flex flex-col justify-between flex-grow">
                    <Title level={4} className="truncate">
                      {item.tenPhong}
                    </Title>
                    <div className="space-x-3 truncate max-w-sm">
                      {trueAttributes.map((attribute, attrIndex) => (
                        <span
                          key={attrIndex}
                          className="tag-item border-2 rounded-md bg-gray-100 p-1"
                        >
                          {attribute}
                        </span>
                      ))}
                    </div>
                    <Text
                      type="danger"
                      className="flex justify-end italic font-semibold"
                    >
                      {item.giaTien}$ / đêm
                    </Text>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="grid-cols-1">
          <Maps />
        </div>
      </div>
      <Footer />
      <Modal
        footer={[
          <Button key="back" onClick={handleCancel}>
            Hủy
          </Button>,
          <Button
            key="submit"
            onClick={() => {
              navigate("/login");
            }}
          >
            Đăng nhập
          </Button>,
        ]}
        title="Hãy đăng nhập để tiếp tục"
        open={open}
        onCancel={() => setOpen(false)}
      >
        <p>Bạn cần phải đăng nhập để xem chi tiết và đặt phòng</p>
      </Modal>
    </div>
  );
}
