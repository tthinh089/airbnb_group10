import React from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsiveUserPage";
import DanhSachPhongDesktop from "./DanhSachPhongDesktop";
import DanhSachPhongTablet from "./DanhSachPhongTablet";
import DanhSachPhongMobile from "./DanhSachPhongMobile";

export default function DanhSachPhong() {
  return (
    <div>
      <Desktop>
        <DanhSachPhongDesktop />
      </Desktop>
      <Tablet>
        <DanhSachPhongTablet />
      </Tablet>
      <Mobile>
        <DanhSachPhongMobile />
      </Mobile>
    </div>
  );
}
