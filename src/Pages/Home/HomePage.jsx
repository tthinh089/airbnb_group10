import React from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsiveUserPage";
import HomePageDesktop from "./HomePageDesktop";
import HomePageTablet from "./HomePageTablet";
import HomePageMobile from "./HomePageMobile";

export default function HomePage() {
  return (
    <div>
      <Desktop>
        <HomePageDesktop />
      </Desktop>
      <Tablet>
        <HomePageTablet />
      </Tablet>
      <Mobile>
        <HomePageMobile />
      </Mobile>
    </div>
  );
}
