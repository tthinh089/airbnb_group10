import React, { useEffect, useState } from "react";
import { Carousel, Typography, message } from "antd";
import { https } from "../../Services/config";
import "animate.css";
import { NavLink } from "react-router-dom";

const contentStyle = {
  width: "100%",
  height: 620,
  overflow: "hidden",
};

const { Title } = Typography;

export default function HomePageDesktop() {
  const [viTri, setVitri] = useState([]);
  const [test, setTest] = useState("Hello");
  useEffect(() => {
    https
      .get("api/vi-tri")
      .then((res) => {
        setVitri(res.data.content);
      })
      .catch((err) => {
        message.error("Lấy thông tin thất bại!");
      });
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      const section = document.querySelector(".everywhere");
      if (section) {
        const sectionTop = section.getBoundingClientRect().top;
        const windowHeight = window.innerHeight;
        if (sectionTop < windowHeight * 0.75) {
          const images = section.querySelectorAll("img");
          images.forEach((image, index) => {
            image.classList.add(
              `animate__animated`,
              `animate__fadeInUp`,
              `animate__delay-${index + 1}s`
            );
          });
        }
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const randomDriveTime = () => {
    return Math.floor(Math.random() * 100) + 30;
  };
  return (
    <div className="z-0">
      <Carousel autoplay={true} className="relative">
        <div>
          <img
            style={contentStyle}
            src="https://news.airbnb.com/wp-content/uploads/sites/4/2020/12/Airbnb-Stay-New-South-Wales.jpg"
            alt=""
          />
        </div>
        <div>
          <img
            style={contentStyle}
            src="https://news.airbnb.com/wp-content/uploads/sites/4/2022/09/Newsroom-About.jpg?fit=5500%2C2361"
            alt=""
          />
        </div>
        <div>
          <img
            style={contentStyle}
            src="https://news.airbnb.com/wp-content/uploads/sites/4/2019/06/PJM020719Q202_Luxe_WanakaNZ_LivingRoom_0264-LightOn_R1.jpg?fit=2500%2C1666"
            alt=""
          />
        </div>
        <div>
          <img
            style={contentStyle}
            src="https://assets.cntraveller.in/photos/632c2f4a67d00ef33fe822d4/16:9/w_2112,h_1188,c_limit/Airbnb.jpg"
            alt=""
          />
        </div>
      </Carousel>
      <div className="absolute inset-x-0 bg-gray-900 p-2 text-center">
        <Title level={3} type="warning">
          {test}
        </Title>
      </div>
      <div className="container mx-auto pt-32">
        <Title level={2}>Khám phá những điểm đến gần đây</Title>
        <div className="grid grid-cols-4 gap-4">
          {viTri.map((item, index) => {
            const driveTime = randomDriveTime();
            return (
              <NavLink
                to={`/diaDiem/${item.id}`}
                key={index}
                className="flex justify-start space-x-2 items-center shadow-md rounded-2xl hover:scale-105 transition duration-200"
              >
                <img
                  src={item.hinhAnh}
                  alt="diaDiem"
                  className="w-24 h-20 object-cover rounded-2xl"
                />
                <div>
                  <Title level={4}>{item.tenViTri}</Title>
                  <Title level={5} type="secondary">
                    {driveTime} giờ lái xe
                  </Title>
                </div>
              </NavLink>
            );
          })}
        </div>
      </div>
      <div className="container mx-auto pt-20 everywhere">
        <Title level={2}>Ở bất cứ đâu</Title>
        <div class="grid grid-cols-4 gap-4">
          <div class="relative">
            <img
              class="w-80 h-52 rounded-lg"
              src="https://www.peerspace.com/resources/wp-content/uploads/Modernist-home-with-pool-roof-deck-santa-monica-los-angeles-rental.jpg"
              alt=""
            />

            <div class="absolute inset-0 flex items-center justify-center opacity-0 hover:opacity-100 transition-opacity">
              <h4 class="text-white text-center text-xl bg-black bg-opacity-70 p-2 rounded-lg">
                Toàn bộ nhà
              </h4>
            </div>
          </div>
          <div class="relative">
            <img
              class="w-80 h-52 rounded-lg"
              src="https://www.travelinglifestyle.net/wp-content/uploads/2022/10/Airbnb-Announces-Winners-Of-the-OMG-Category.jpg"
              alt=""
            />

            <div class="absolute inset-0 flex items-center justify-center opacity-0 hover:opacity-100 transition-opacity">
              <h4 class="text-white text-center text-xl bg-black bg-opacity-70 p-2 rounded-lg">
                Chổ ở độc đáo
              </h4>
            </div>
          </div>
          <div class="relative">
            <img
              class="w-80 h-52 rounded-lg"
              src="https://www.travelandleisure.com/thmb/U_ek9iFrp3UwNSOf5xW7MsTEV88=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/header-zion-ecocabin-WISHLISTBNB0222-f2649df02ff748c5bcff43052f20e309.jpg"
              alt=""
            />

            <div class="absolute inset-0 flex items-center justify-center opacity-0 hover:opacity-100 transition-opacity">
              <h4 class="text-white text-center text-xl bg-black bg-opacity-70 p-2 rounded-lg">
                Trang trại và thiên nhiên
              </h4>
            </div>
          </div>
          <div class="relative">
            <img
              class="w-80 h-52 rounded-lg"
              src="https://news.airbnb.com/wp-content/uploads/sites/4/2021/08/1.The-Philippines-most-famous-Smiling-Dog-Luffy-B-unveils-his-wishlist-of-pet-friendly-Airbnb-Stays_1-2.jpg?fit=1748%2C1200"
              alt=""
            />

            <div class="absolute inset-0 flex items-center justify-center opacity-0 hover:opacity-100 transition-opacity">
              <h4 class="text-white text-center text-xl bg-black bg-opacity-70 p-2 rounded-lg">
                Cho phép thú cưng
              </h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
