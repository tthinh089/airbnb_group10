import React from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsiveUserPage";
import AccountPageDesktop from "./AccountPageDesktop";
import AccountPageTablet from "./AccountPageTablet";
import AccountPageMobile from "./AccountPageMobile";

export default function AccountPage() {
  return (
    <div>
      <Desktop>
        <AccountPageDesktop />
      </Desktop>
      <Tablet>
        <AccountPageTablet />
      </Tablet>
      <Mobile>
        <AccountPageMobile />
      </Mobile>
    </div>
  );
}
