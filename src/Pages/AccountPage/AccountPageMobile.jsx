import React, { useEffect, useState } from "react";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import { useDispatch, useSelector } from "react-redux";
import {
  Avatar,
  Button,
  Divider,
  Input,
  Form,
  Modal,
  Typography,
  Upload,
  message,
  Select,
  DatePicker,
  Radio,
} from "antd";
import {
  CheckCircleOutlined,
  CheckOutlined,
  UploadOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { https } from "../../Services/config";
import { NavLink } from "react-router-dom";
import axios from "axios";
import { CYBER_TOKEN } from "../../utils/config";
import { setLogin, updateImageUrl } from "../../redux/userSlice";

export default function AccountPageMobile() {
  let dispatch = useDispatch();
  const user = useSelector((state) => state.userSlice.userInfo);
  const token = useSelector((state) => state.userSlice.tokenUser);
  const handleFileChange = (info) => {
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  };
  const customRequest = async ({ file, onSuccess, onError }) => {
    try {
      const formData = new FormData();
      formData.append("formFile", file);
      const response = await https.post("/api/users/upload-avatar", formData, {
        headers: {
          token: token,
        },
      });
      console.log(response);
      const userJSON = localStorage.getItem("DATA_LOCAL");
      const user = JSON.parse(userJSON);
      user.avatar = response.data.content.avatar;
      localStorage.setItem("DATA_LOCAL", JSON.stringify(user));
      dispatch(updateImageUrl(user.avatar));
      setAvatarModal(false);
      onSuccess();
    } catch (error) {
      console.error(error);
      onError(error);
    }
  };

  const uploadProps = {
    customRequest,
    onChange: handleFileChange,
    multiple: false,
  };
  const [infoModal, setInfoModal] = useState(false);
  const [avatarModal, setAvatarModal] = useState(false);
  const showModal = () => {
    setAvatarModal(true);
  };
  const showInfoModal = () => {
    setInfoModal(true);
  };
  const handleOk = () => {
    setAvatarModal(false);
    setInfoModal(false);
  };
  const handleCancel = () => {
    setAvatarModal(false);
    setInfoModal(false);
  };

  const onFinish = (values) => {
    console.log("Success:", values);
    const url = `https://airbnbnew.cybersoft.edu.vn/api/users/${user.id}`;
    const headers = {
      tokenCybersoft: CYBER_TOKEN,
    };

    axios
      .put(url, values, { headers })
      .then((res) => {
        message.success("Cập nhật thành công");

        const userJSON = localStorage.getItem("DATA_LOCAL");
        let user = JSON.parse(userJSON);
        user = res.data.content;
        localStorage.setItem("DATA_LOCAL", JSON.stringify(user));
        dispatch(setLogin(user));
        setInfoModal(false);
      })
      .catch((err) => {
        message.error("Cập nhật thất bại");
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const [userBooking, setUserBooking] = useState([]);
  const dayjs = require("dayjs");
  const { Text, Title } = Typography;

  useEffect(() => {
    https
      .get(`/api/dat-phong/lay-theo-nguoi-dung/${user.id}`)
      .then((res) => {
        setUserBooking(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
        message.error("Lấy thông tin thất bại!");
      });
  }, []);

  return (
    <div>
      <Header />
      <div className="container pt-5 mx-auto flex space-x-6">
        <div className="w-full">
          <div className="w-full flex flex-col items-center space-y-3">
            <Title level={1}>
              Xin chào <span className="text-purple-700">{user.name}</span>!
            </Title>
            <Text className="font-semibold" type="secondary">
              Bắt đầu tham gia vào 2023
            </Text>
            <Text
              className="font-semibold underline cursor-pointer text-lg"
              onClick={showInfoModal}
            >
              Chỉnh sửa hồ sơ
            </Text>
            <div className="border-2 items-center h-fit space-y-3 p-3 rounded-lg w-3/4">
              <div className="flex flex-col items-center">
                {user.avatar !== "" ? (
                  <img
                    src={user.avatar}
                    className="w-20 rounded-full border-2"
                    alt="avatar"
                  />
                ) : (
                  <Avatar
                    size="large"
                    className="flex items-center justify-center w-40 h-40"
                    icon={<UserOutlined />}
                  />
                )}
                <Text
                  className="font-semibold underline cursor-pointer"
                  onClick={showModal}
                >
                  Cập nhật ảnh
                </Text>
              </div>
              <div className="flex flex-col">
                <div className="flex space-x-2">
                  <Title level={3}>Xác nhận danh tính</Title>
                  <CheckCircleOutlined className="text-xl" />
                </div>
                <Text className="font-semibold" type="secondary">
                  Xác nhận danh tính của bạn với huy hiệu xác minh danh tính
                </Text>
                <Button>Nhận huy hiệu</Button>
                <Divider />
                <Title level={3}>{user.name} đã xác thực</Title>
                <div className="flex justify-start items-center space-x-3">
                  <CheckOutlined />
                  <Text className="font-semibold" type="secondary">
                    Địa chỉ email
                  </Text>
                </div>
              </div>
            </div>
          </div>
          <Title level={2} className="pt-12">
            Phòng đã thuê
          </Title>
          <div className="grid-cols-1 space-y-3">
            {userBooking.length !== 0 ? (
              userBooking.map((room) => {
                const formatNgayDen = dayjs(room.ngayDen).format("DD-MM-YYYY");
                const formatNgayDi = dayjs(room.ngayDi).format("DD-MM-YYYY");
                return (
                  <NavLink
                    to={`/diaDiem/1/${room.maPhong}`}
                    className="border-2 rounded-lg p-3 flex items-center justify-around cursor-pointer"
                    key={room.id}
                  >
                    <Title level={4}>Mã phòng: {room.maPhong}</Title>
                    <Text className="font-semibold" type="secondary">
                      Ngày nhận phòng: {formatNgayDen}
                    </Text>
                    <Text className="font-semibold" type="secondary">
                      Ngày trả phòng: {formatNgayDi}
                    </Text>
                    <Text className="font-semibold">
                      Số khách: {room.soLuongKhach}
                    </Text>
                  </NavLink>
                );
              })
            ) : (
              <Text className="font-semibold italic" type="secondary">
                Có vẻ như bạn chưa đặt phòng nào
              </Text>
            )}
          </div>
        </div>
      </div>
      <Footer />
      <Modal
        title="Cập nhật ảnh đại diện"
        open={avatarModal}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <>
            <Button onClick={handleCancel}>Hủy</Button>
            <Button
              onClick={handleOk}
              className="bg-purple-700 hover:bg-purple-600 text-white font-semibold"
            >
              Đồng ý
            </Button>
          </>,
        ]}
      >
        <div className="flex flex-col items-center">
          <Upload {...uploadProps}>
            <Button icon={<UploadOutlined />}>Upload Image</Button>
          </Upload>
        </div>
      </Modal>
      <Modal
        title="Cập nhật thông tin"
        open={infoModal}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[]}
      >
        <Form
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          labelCol={{ span: 4 }}
        >
          <Form.Item
            label="Name"
            name="name"
            initialValue={user.name}
            rules={[
              {
                required: true,
                message: "Please input your name!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            initialValue={user.email}
            rules={[
              {
                required: true,
                type: "email",
                message: "Please enter a valid email address!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Phone"
            name="phone"
            initialValue={user.phone}
            rules={[
              {
                required: true,
                message: "Please input your phone number!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item label="Birthday" name="birthday">
            <DatePicker size="large" />
          </Form.Item>

          <Form.Item
            label="Gender"
            name="gender"
            initialValue={user.gender}
            valuePropName="checked"
          >
            <Radio.Group>
              <Radio.Button value={true}>Male</Radio.Button>
              <Radio.Button value={false}>Female</Radio.Button>
            </Radio.Group>
          </Form.Item>

          <Form.Item label="Role" name="role" initialValue={user.role}>
            <Select disabled>
              <Select.Option value="admin">ADMIN</Select.Option>
              <Select.Option value="user">USER</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 20 }}>
            <Button htmlType="submit">Submit</Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
