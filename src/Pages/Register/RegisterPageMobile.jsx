import React from 'react'
import bgRegister from "../../assets/image/bg-register-airbnb.png";
import { Button, Form, Input, Select, DatePicker, message } from "antd";
import moment from "moment";
import { NavLink, useNavigate } from "react-router-dom";
import { https } from "../../Services/config";
import style from "../../assets/style/style.css"
const { Option } = Select;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};
export default function RegisterPageMobile() {
 const navigate = useNavigate();
  const [form] = Form.useForm();
  const onFinish = (values) => {
    values["birthday"] = moment(values["birthday"]).format(
      "YYYY-MM-DD" || "DD/MM/YYYY"
    );
    https
      .post("/api/auth/signup", values)
      .then((res) => {
        message.success("Đăng ký thành công");
        setTimeout(() => {
          navigate("/");
        }, 2000);
      })
      .catch((err) => {
        message.error(err.response.data.content)
      });
  };
  return (
    <div
      className="flex items-center justify-center max-w-full h-screen bg-no-repeat bg-cover"
      style={{ backgroundImage: `url(${bgRegister})` }}
    >
      <div className="h-4/6 w-4/5">
        <Form
          {...formItemLayout}
          form={form}
          name="register"
          onFinish={onFinish}
          initialValues={{
            residence: ["zhejiang", "hangzhou", "xihu"],
            prefix: "86",
          }}
          className="w-full font-medium  bg-red-100  p-5  rounded-lg"
          scrollToFirstError
              >
            <h2 className="text-xl font-bold text-purple-600 text-center mb-5">Register Airbnb</h2>
          <Form.Item
            className='mb-3'
            name="name"
            label="User name"
            tooltip="What is your account name?"
            rules={[
              {
                required: true,
                message: "Please input your  account name!",
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            className='mb-3'
            name="email"
            label="E-mail"
            rules={[
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
              {
                required: true,
                message: "Please input your E-mail!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            className='mb-3'
            name="password"
            label="Password"
            rules={[
              { type: "string",
                pattern: new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/), 
              message: "Password at least 6 characters (letters & numbers)"
            },
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            className='mb-3'
            name="phone"
            label="Phone Number"
            rules={[
              {
                type: "string",
                pattern: new RegExp(/\d+/g),
                message: "The input is not valid phone ",
              },
              {
                required: true,
                message: "Please input your phone number!",
              },
            ]}
          >
            <Input
              style={{
                width: "100%",
              }}
            />
          </Form.Item>
            <Form.Item
            className='mb-3'
            label="Your birthday"
            name="birthday"
            rules={[
              {
                type: "date",
                required: "true",
                message: "Please input your birthday!",
              },
            ]}
          >
            <DatePicker className="w-full" />
          </Form.Item>
            <Form.Item
            className='mb-3'
            name="gender"
            label="Gender"
            rules={[
              {
                required: true,
                message: "Please select gender!",
              },
            ]}
          >
            <Select placeholder="select your gender">
              <Option value="true">Male</Option>
              <Option value="false">Female</Option>
            </Select>
          </Form.Item>

          <Form.Item {...tailFormItemLayout} className="text-end mb-3">
            <Button
              className="btnGloble btnGlobleMobile"
              htmlType="submit"
            >
              Register
            </Button>
          </Form.Item>
          <h4 className="text-center font-medium text-base ">
            Don't have an account yet? 
            <NavLink to={"/"} className="text-red-500 underline ml-1">
              Register now
            </NavLink>
          </h4>
        </Form>
      </div>
    </div>
  );
}
