import React from 'react'
import { Desktop, Mobile, Tablet } from '../../responsives/responsive'
import RegisterPageDesktop from './RegisterPageDesktop'
import RegisterPageTablet from './RegisterPageTablet'
import RegisterPageMobile from './RegisterPageMobile'

export default function RegisterPage() {
  return (
      <>
          <Desktop>
              <RegisterPageDesktop/>
          </Desktop>
          <Tablet>
              <RegisterPageTablet/>
          </Tablet>
          <Mobile>
              <RegisterPageMobile/>
          </Mobile>
      </>
  )
}
