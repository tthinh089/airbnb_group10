import React from "react";
import "./Page404.css";
import { NavLink } from "react-router-dom";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";

export default function Page404() {
  return (
    <div>
      <Header />
      <div className="pt-20 container mx-auto">
        <section class="page_404">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 ">
                <div class="col-sm-10 col-sm-offset-1  text-center">
                  <div class="four_zero_four_bg">
                    <h1 class="text-center ">404</h1>
                  </div>
                  <div class="contant_box_404">
                    <h3 class="h2">Look like you're lost</h3>
                    <p>the page you are looking for not avaible!</p>
                    <a class="link_404">
                      <NavLink to="/">Go to Home</NavLink>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  );
}
