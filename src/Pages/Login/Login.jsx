import React from "react";
import { Button, Form, Input, message } from "antd";
import Lottie from "lottie-react";
import { https } from "../../Services/config";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { localServ } from "../../Services/localServ";
import { setLogin } from "../../redux/userSlice";
import homeAnimate from "./homeAnimate.json";

const onFinishFailed = (errorInfo) => {
  console.log("Failed:", errorInfo);
};

export default function Login() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    https
      .post("/api/auth/signin", values)
      .then((res) => {
        message.success("Đăng nhập thành công!");
        console.log(res.data.content);
        localServ.setUser({ ...res.data.content.user });
        localServ.setTokenUser(res.data.content.token);
        dispatch(setLogin(res.data.content.user));
        setTimeout(() => {
          navigate("/");
        }, 2000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng nhập thất bại!");
      });
  };
  return (
    <div
      className="flex justify-center items-center"
      style={{
        backgroundImage: `url(
          "https://wallpapercave.com/wp/wp10784413.jpg"
        )`,
        backgroundSize: "150%",
        backgroundPosition: "center",
        height: "100vh",
        width: "100vw",
        objectFit: "cover",
      }}
    >
      <div
        className="container p-10 rounded flex justify-center items-center max-w-6xl"
        style={{ backgroundColor: "rgba(0, 0, 0, 0.8)" }}
      >
        <div className="h-full w-1/2">
          <Lottie
            style={{ height: 400 }}
            animationData={homeAnimate}
            loop={true}
          />
        </div>
        <div className=" h-full w-1/2">
          <h1 className="text-white my-5 text-3xl">Đăng nhập</h1>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 18,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            >
              <Input
                style={{
                  height: 50,
                }}
                placeholder="Email"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password style={{ height: 50 }} placeholder="Mật khẩu" />
            </Form.Item>
            <Form.Item>
              <h2 className="text-white">
                Chưa có tài khoản?{" "}
                <NavLink to="/register" className="text-red-500 underline">
                  Đăng ký ngay
                </NavLink>
              </h2>
            </Form.Item>
            <Form.Item
              wrapperCol={{
                span: 18,
              }}
            >
              <div style={{ display: "flex", justifyContent: "center" }}>
                <Button
                  onMouseEnter={(e) => (e.target.style.fontSize = "18px")}
                  onMouseLeave={(e) => (e.target.style.fontSize = "16px")}
                  htmlType="submit"
                  style={{
                    flex: 1,
                    height: 50,
                    backgroundColor: "blueviolet",
                    color: "white",
                    fontWeight: "bold",
                  }}
                >
                  Đăng nhập
                </Button>
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
