import React from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsiveUserPage";
import ChiTietPhongDesktop from "./ChiTietPhongDesktop";
import ChiTietPhongTablet from "./ChiTietPhongTablet";
import ChiTietPhongMobile from "./ChiTietPhongMobile";

export default function ChiTietPhong() {
  return (
    <div>
      <Desktop>
        <ChiTietPhongDesktop />
      </Desktop>
      <Tablet>
        <ChiTietPhongTablet />
      </Tablet>
      <Mobile>
        <ChiTietPhongMobile />
      </Mobile>
    </div>
  );
}
