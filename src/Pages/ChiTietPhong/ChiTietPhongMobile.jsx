import React, { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { https } from "../../Services/config";
import {
  Avatar,
  Button,
  DatePicker,
  Divider,
  Dropdown,
  Form,
  Menu,
  Typography,
  message,
} from "antd";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import {
  HomeOutlined,
  CrownOutlined,
  FieldTimeOutlined,
  SafetyCertificateOutlined,
  UserOutlined,
  DownOutlined,
  MinusCircleOutlined,
  PlusCircleOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { MdIron, MdPool } from "react-icons/md";
import { GiGasStove, GiWashingMachine } from "react-icons/gi";
import { FaWind } from "react-icons/fa";
import { AiFillCar, AiOutlineWifi } from "react-icons/ai";
import { PiTelevisionLight } from "react-icons/pi";
import { TbIroning2 } from "react-icons/tb";
import { useSelector } from "react-redux";
import TextArea from "antd/es/input/TextArea";
import ButtonGroup from "antd/es/button/button-group";

export default function ChiTietPhongMobile() {
  const { roomID } = useParams();
  const [buttonLoading, setButtonLoading] = useState(false);
  const  user  = useSelector((state) => state.userSlice.userInfo);
  const { thoiGian } = useSelector((state) => state.headerSlice.headerInfo);
  const { soKhach } = useSelector((state) => state.headerSlice.headerInfo);
  const [totalGuest, setTotalGuest] = useState(soKhach ? soKhach : 1);
  const userToken = useSelector((state) => state.userSlice.tokenUser);
  const dayjs = require("dayjs");
  const currentDate = dayjs().format("YYYY-MM-DD");
  const tomorrow = dayjs().add(1, "day").format("YYYY-MM-DD");
  const disabledDate = (current) => {
    return current && current < dayjs().startOf("day");
  };

  const calculateNumberOfDays = (startDate, endDate) => {
    const start = dayjs(startDate, "YYYY-MM-DD");
    const end = dayjs(endDate, "YYYY-MM-DD");
    return end.diff(start, "day");
  };

  const [selectInfo, setSelectInfo] = useState({
    maNguoiDung: user.id,
    maPhong: roomID,
    ngayDen: thoiGian ? thoiGian.nhanPhong : currentDate,
    ngayDi: thoiGian ? thoiGian.traPhong : tomorrow,
    soLuongKhach: totalGuest,
    soNgay: calculateNumberOfDays(
      thoiGian ? thoiGian.nhanPhong : currentDate,
      thoiGian ? thoiGian.traPhong : tomorrow
    ),
  });

  const onFinish = (values) => {
    setButtonLoading(true);
    https
      .post("/api/dat-phong", selectInfo)
      .then((res) => {
        setTimeout(() => {
          setButtonLoading(false);
          message.success("Đặt phòng thành công");
        }, 1000);
      })
      .catch((err) => {
        setTimeout(() => {
          setButtonLoading(false);
          console.log(err);
          message.error("Đặt phòng thất bại");
        }, 1000);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onNhanPhong = (value) => {
    if (value) {
      const formatDate = dayjs(value).format("YYYY-MM-DD");
      const ngayDen = dayjs(formatDate, "YYYY-MM-DD");
      const ngayDi = dayjs(selectInfo.ngayDi, "YYYY-MM-DD");
      if (
        (ngayDi.isValid() && ngayDen.isSame(ngayDi)) ||
        ngayDen.isAfter(ngayDi)
      ) {
        message.error(
          "Ngày nhận phòng không thể lớn hơn hoặc bằng ngày trả phòng!"
        );
        return currentDate;
      }
      setSelectInfo((prevInfo) => ({
        ...prevInfo,
        ngayDen: formatDate,
        soNgay: calculateNumberOfDays(formatDate, prevInfo.ngayDi),
      }));
    }
  };

  const onTraPhong = (value) => {
    if (value) {
      const formatDate = dayjs(value).format("YYYY-MM-DD");
      setSelectInfo((prevInfo) => ({
        ...prevInfo,
        ngayDi: formatDate,
        soNgay: calculateNumberOfDays(prevInfo.ngayDen, formatDate),
      }));
    }
  };

  const handleGuestSelect = (count) => {
    setTotalGuest(count);
    setSelectInfo({
      ...selectInfo,
      soLuongKhach: count,
    });
  };
  const [comment, setComment] = useState({
    id: 0,
    maPhong: roomID,
    maNguoiBinhLuan: user.id,
    ngayBinhLuan: currentDate,
    noiDung: "test comment",
    saoBinhLuan: 0,
  });

  const onChange = (e) => {
    setComment((prevState) => ({
      ...prevState,
      noiDung: e.target.value,
    }));
  };

  const { Text, Title } = Typography;

  const fetchCommentList = useCallback(() => {
    https
      .get(`/api/binh-luan/lay-binh-luan-theo-phong/${roomID}`)
      .then((res) => {
        setCommentList(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
        message.error("Lấy thông tin thất bại!");
      });
  }, [roomID]);

  useEffect(() => {
    fetchCommentList();
  }, [roomID, fetchCommentList]);

  const submitComment = () => {
    https
      .post("/api/binh-luan", comment, {
        headers: {
          token: userToken,
        },
      })
      .then((res) => {
        console.log(res);
        fetchCommentList();
        message.success("Đăng bình luận thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng bình luận thất bại");
      });
  };
  const [commentList, setCommentList] = useState([]);
  const [attributes, setAttributes] = useState([]);
  const [roomInfo, setRoomInfo] = useState([]);
  const [ownerName, setOwnerName] = useState(null);
  useEffect(() => {
    https
      .get(`/api/phong-thue/${roomID}`)
      .then((res) => {
        setRoomInfo(res.data.content);
        const room = res.data.content;
        const regex = /(.+)(?= là Chủ nhà siêu cấp)/;
        const match = res.data.content.moTa.match(regex);
        const ownerName = match ? match[1].trim() : "";
        setOwnerName(ownerName);
        const trueAttributes = [];
        if (room.banLa) {
          trueAttributes.push("Bàn là");
        }
        if (room.banUi) {
          trueAttributes.push("Bàn ủi");
        }
        if (room.bep) {
          trueAttributes.push("Bếp");
        }
        if (room.dieuHoa) {
          trueAttributes.push("Điều hòa");
        }
        if (room.doXe) {
          trueAttributes.push("Đỗ xe");
        }
        if (room.hoBoi) {
          trueAttributes.push("Hồ bơi");
        }
        if (room.mayGiat) {
          trueAttributes.push("Máy giặt");
        }
        if (room.tivi) {
          trueAttributes.push("Ti vi");
        }
        if (room.wifi) {
          trueAttributes.push("WiFi");
        }
        setAttributes(trueAttributes);
      })
      .catch((err) => {
        console.log("err: ", err);
        message.error("Lấy thông tin thất bại!");
      });
  }, []);

  const attributeIcons = {
    "Bàn là": <MdIron />,
    "Bàn ủi": <TbIroning2 />,
    Bếp: <GiGasStove />,
    "Điều hòa": <FaWind />,
    "Đỗ xe": <AiFillCar />,
    "Hồ bơi": <MdPool />,
    "Máy giặt": <GiWashingMachine />,
    "Ti vi": <PiTelevisionLight />,
    WiFi: <AiOutlineWifi />,
  };
  return (
    <div>
      <Header />
      <div className="container mx-auto pt-10 space-y-3">
        <Title level={2}>{roomInfo.tenPhong}</Title>
        <img src={roomInfo.hinhAnh} alt="room img" className="rounded-lg" />
        <div className="">
          <div className="flex">
            <div className="">
              <div className="space-y-4 ">
                <div className="flex justify-between">
                  <div className="">
                    <Title level={4}>
                      Toàn bộ căn hộ {roomInfo.tenPhong}. Chủ nhà:{" "}
                      <span className="font-bold text-purple-800">
                        {ownerName ? ownerName : "Anonymous"}
                      </span>
                      .
                    </Title>
                    <Text className="font-semibold">
                      {roomInfo.khach} khách - {roomInfo.phongNgu} phòng ngủ -
                      {"  "}
                      {roomInfo.giuong} giường ngủ - {roomInfo.phongTam} phòng
                      tắm
                    </Text>
                  </div>
                  <img
                    src="https://i.pravatar.cc/"
                    alt="avartar"
                    className="rounded-full border-2 w-20 h-20"
                  />
                </div>
                <Divider className="border-t-2 border-gray-300" />
                <div className="flex space-x-4">
                  <HomeOutlined className="text-2xl flex items-center" />
                  <div className="">
                    <Title level={4}>Toàn bộ nhà</Title>
                    <Text className="font-semibold" type="secondary">
                      Bạn sẽ có chung cư cao cấp cho riêng mình
                    </Text>
                  </div>
                </div>
                <div className="flex space-x-4">
                  <SafetyCertificateOutlined className="text-2xl flex items-center" />
                  <div className="">
                    <Title level={4}>Vệ sinh tăng cường</Title>
                    <Text className="font-semibold" type="secondary">
                      Chủ nhà này đã cam kết thực hiện vệ sinh tăng cường 5 bước
                      của Airbnb
                    </Text>
                  </div>
                </div>
                <div className="flex space-x-4">
                  <CrownOutlined className="text-2xl flex items-center" />
                  <div className="">
                    <Title level={4}>
                      {" "}
                      {ownerName ? ownerName : "Anonymous"} là chủ nhà siêu cấp
                    </Title>
                    <Text className="font-semibold" type="secondary">
                      Chủ nhà siêu cấp là những chủ nhà có kinh nghiệm được đánh
                      giá cao và là những người cam kết mang lại quãng thời gian
                      tuyệt vời cho khách hàng.
                    </Text>
                  </div>
                </div>
                <div className="flex space-x-4">
                  <FieldTimeOutlined className="text-2xl flex items-center" />
                  <div className="">
                    <Title level={4}>Miễn phí hủy trong 48h</Title>
                  </div>
                </div>
                <Divider className="border-t-2 border-gray-300" />
                <Title level={4}>Mô tả căn hộ:</Title>
                <Text className="font-semibold">{roomInfo.moTa}</Text>
              </div>
              <Divider className="border-t-2 border-gray-300" />
              <Title level={4}>Tiện nghi</Title>
              <div className="grid grid-cols-2">
                {attributes.map((attribute) => (
                  <div
                    key={attribute}
                    className="flex gap-4 items-center space-y-3"
                  >
                    <span className="attribute-icon text-3xl">
                      {attributeIcons[attribute]}
                    </span>
                    <span className="w-full m-0">{attribute}</span>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="border-2 rounded-xl p-3 mt-16 h-fit">
            <Title level={3}>{roomInfo.giaTien}$ / đêm</Title>
            <Form
              className="flex flex-col items-center"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <div className="flex">
                <Form.Item>
                  <div className="flex flex-col border-2 rounded-lg p-2">
                    <span className="font-bold">Nhận phòng</span>
                    <DatePicker
                      size="large"
                      defaultValue={dayjs(
                        thoiGian ? thoiGian.nhanPhong : currentDate,
                        "YYYY-MM-DD"
                      )}
                      format="YYYY-MM-DD"
                      disabledDate={disabledDate}
                      onChange={onNhanPhong}
                    />
                  </div>
                </Form.Item>
                <Form.Item>
                  <div className="flex flex-col border-2 rounded-lg p-2">
                    <span className="font-bold">Trả phòng</span>
                    <DatePicker
                      size="large"
                      defaultValue={dayjs(
                        thoiGian ? thoiGian.traPhong : tomorrow,
                        "YYYY-MM-DD"
                      )}
                      format="YYYY-MM-DD"
                      disabledDate={(current) =>
                        current &&
                        current <= dayjs(selectInfo.ngayDen, "DD-MM-YYYY")
                      }
                      onChange={onTraPhong}
                    />
                  </div>
                </Form.Item>
              </div>
              <Form.Item className="w-80 p-2 border-2 rounded-lg flex justify-center">
                <>
                  <Dropdown
                    trigger={["click"]}
                    placement="bottomRight"
                    overlay={
                      <Menu>
                        <Menu.Item key="guest">
                          <span className="mr-2">Số khách</span>
                          <ButtonGroup className="space-x-3">
                            <Button
                              icon={<MinusCircleOutlined />}
                              onClick={() => handleGuestSelect(totalGuest - 1)}
                              disabled={totalGuest === 1}
                            />
                            <span className="flex items-center">
                              {totalGuest}
                            </span>
                            <Button
                              icon={<PlusCircleOutlined />}
                              onClick={() => handleGuestSelect(totalGuest + 1)}
                            />
                          </ButtonGroup>
                        </Menu.Item>
                      </Menu>
                    }
                  >
                    <a
                      className="ant-dropdown-link flex items-center gap-2"
                      onClick={(e) => e.preventDefault()}
                    >
                      {`${totalGuest} khách`} <DownOutlined />
                    </a>
                  </Dropdown>
                </>
              </Form.Item>
              <Form.Item className="w-80">
                <div>
                  {!buttonLoading ? (
                    <Button
                      className="w-full bg-purple-800 text-white font-bold hover:bg-purple-700 h-10"
                      htmlType="submit"
                    >
                      Đặt phòng
                    </Button>
                  ) : (
                    <Button
                      disabled
                      className="w-full bg-purple-800 text-white font-bold h-10"
                      htmlType="submit"
                    >
                      Đặt phòng
                      <LoadingOutlined />
                    </Button>
                  )}
                </div>
              </Form.Item>
              <Form.Item>
                <div className="w-72 flex justify-between font-semibold">
                  <Text
                    type="secondary"
                    className="flex justify-start underline"
                  >
                    {roomInfo.giaTien}$ x {selectInfo.soNgay} đêm:
                  </Text>
                  <Text type="secondary" className="flex justify-start">
                    {roomInfo.giaTien * selectInfo.soNgay}$
                  </Text>
                </div>
              </Form.Item>
              <Form.Item>
                <div className="w-72 flex justify-between font-semibold">
                  <Text
                    type="secondary"
                    className="flex justify-start underline"
                  >
                    Phí dịch vụ 10%:
                  </Text>
                  <Text type="secondary" className="flex justify-start">
                    {(roomInfo.giaTien * selectInfo.soNgay * 10) / 100}$
                  </Text>
                </div>
              </Form.Item>
              <Form.Item className="w-1/2">
                <Divider />
              </Form.Item>
              <Form.Item>
                <div className="w-72 flex justify-between font-semibold">
                  <Text className="flex justify-start underline">Tổng:</Text>
                  <Text className="flex justify-start">
                    {roomInfo.giaTien * selectInfo.soNgay +
                      (roomInfo.giaTien * selectInfo.soNgay * 10) / 100}
                    $
                  </Text>
                </div>
              </Form.Item>
            </Form>
          </div>
        </div>
        <Divider className="border-t-2 border-gray-300 py-5" />
        <div className="">
          <Title level={4}>Bình luận</Title>
          <div className="flex">
            {user ? (
              <img
                src={user.avatar}
                alt="avatar"
                className="rounded-full w-20 h-20"
              />
            ) : (
              <Avatar
                className="flex items-center justify-center w-20 h-20"
                icon={<UserOutlined className="text-2xl" />}
              />
            )}
            <TextArea rows={4} style={{ resize: "none" }} onChange={onChange} />
          </div>
          <div className="flex justify-end">
            <Button className="w-28 my-2" onClick={submitComment}>
              Bình luận
            </Button>
          </div>
        </div>
        <div className="grid grid-cols-1">
          {commentList.map((comment) => {
            return (
              <div className="space-y-4 border-2 rounded-lg mb-3 mx-2">
                <div className="flex gap-4 mt-2 ml-2">
                  {comment.avatar ? (
                    <img
                      src={comment.avatar}
                      className="rounded-full w-16 h-16 object-cover border-2"
                      alt="avatar"
                    />
                  ) : (
                    <Avatar
                      className="flex items-center justify-center w-16 h-16"
                      icon={<UserOutlined className="text-2xl" />}
                    />
                  )}
                  <div>
                    <Title level={5}>{comment.tenNguoiBinhLuan}</Title>
                    <Text className="font-semibold" type="secondary">
                      {comment.ngayBinhLuan}
                    </Text>
                  </div>
                </div>
                <div className="p-5">
                  <Text className="font-semibold">{comment.noiDung}</Text>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <Footer />
    </div>
  );
}
