import React from "react";
import { FaEye, FaEdit, FaTrashAlt } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import {
  setLocationSelected,
  setMess,
  setTogglePopup,
  setTogglePopupDetail,
  setTogglePopupUpload,
} from "../../../redux/listLocationSlice";
import { https } from "../../../Services/config";
import { message } from "antd";
import { CYBER_TOKEN } from "../../../utils/config";
export default function Location(props) {
  const dispatch = useDispatch();
  let { location } = props;
  let { id, tenViTri, tinhThanh, quocGia, hinhAnh } = location;
  const { tokenUser } = useSelector((state) => state.userSlice);

  const handleDelete = (id) => {
    if (tokenUser) {
      https
        .delete(`/api/vi-tri/${id}`, {
          headers: {
            token: tokenUser,
            tokenCybersoft: CYBER_TOKEN,
          },
        })
        .then((res) => {
          message.success("Xóa thông tin thành công");
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    }
  };

  return (
    <>
      <tr className="text-center md:text-base text-xs">
        <td className="border border-slate-300 md:p-2 ">{id}</td>
        <td className="border border-slate-300 md:p-2 ">{tenViTri}</td>
        <td className="border border-slate-300 md:p-2 ">
          {hinhAnh ? (
            <div className="md:flex md:items-center md:justify-center space-y-2">
              <div className="md:w-9/12">
                <img
                  src={hinhAnh}
                  alt="Hình ảnh"
                  className="w-36 h-auto object-cover"
                />
              </div>
              <div className="md:w-3/12">
                <button
                  className="bg-purple-300 hover:bg-purple-600 text-white py-1 px-2 rounded-sm"
                  onClick={() => {
                    dispatch(setTogglePopupUpload(true));
                    dispatch(setLocationSelected(location));
                  }}
                >
                  {" "}
                  <FaEdit />
                </button>
              </div>
            </div>
          ) : (
            "-"
          )}
        </td>
        <td className="border border-slate-300 md:p-2  truncate">
          {tinhThanh}
        </td>
        <td className="border border-slate-300 md:p-2  truncate">{quocGia}</td>
        <td className="border border-slate-300 md:p-2   space-y-1 ">
          <button
            className=" flex space-x-1 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-700 rounded"
            onClick={() => {
              dispatch(setTogglePopupDetail(true));
              dispatch(setLocationSelected(location));
            }}
          >
            <FaEye /> <span>View Detail</span>
          </button>
          <button
            className="flex space-x-1 bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 border border-yellow-700 rounded"
            onClick={() => {
              dispatch(setMess(false));
              dispatch(setTogglePopup(true));
              dispatch(setLocationSelected(location));
            }}
          >
            <FaEdit />
            Edit
          </button>
          {tokenUser && (
            <button
              className="flex space-x-1 bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-700 rounded"
              onClick={() => {
                handleDelete(`${id}`);
              }}
            >
              <FaTrashAlt />
              <span>Delete</span>
            </button>
          )}
        </td>
      </tr>
    </>
  );
}
