import React, { useEffect, useState } from "react";
import { https } from "../../../Services/config";
import { useDispatch, useSelector } from "react-redux";
import Location from "./Location";
import { setListLocation } from "../../../redux/listLocationSlice";

export default function LocationTable(props) {
  const { listLocation } = useSelector((state) => state.listLocationSlice);
  const dispatch = useDispatch();
  let { recordsPerPage, currentPage } = props;

  useEffect(() => {
    if ((currentPage, recordsPerPage, listLocation)) {
      https
        .get(
          `/api/vi-tri/phan-trang-tim-kiem?pageIndex=${currentPage}&pageSize=${recordsPerPage}`
        )
        .then((res) => {
          dispatch(setListLocation(res.data.content.data));
        })
        .catch((err) => {
          console.log("err", err);
        });
    }
  }, [currentPage, recordsPerPage]);
  const renderList = (list) => {
    return list.map((location) => {
      return <Location key={location.id} location={location} />;
    });
  };
  
  return (
    <div className="horizontal-scroll-except-first-column overflow-auto">
      <table className="table-auto  border-collapse border border-slate-400 w-full overscroll-auto">
        <thead>
          <tr className="text-purple-500 ">
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Mã
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Tên vị trí
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Hình Ảnh
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Tỉnh thành
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Quốc gia
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Actions
            </th>
          </tr>
        </thead>
        <tbody>{renderList(listLocation)}</tbody>
      </table>
    </div>
  );
}
