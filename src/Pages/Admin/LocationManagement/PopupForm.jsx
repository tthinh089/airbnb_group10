import React, { useEffect, useState } from "react";
import Popup from "../../../Components/Popup/Popup";
import "react-datepicker/dist/react-datepicker.css";
import { useDispatch, useSelector } from "react-redux";
import { setTogglePopup } from "../../../redux/listLocationSlice";
import { https } from "../../../Services/config";
import { message } from "antd";
import { CYBER_TOKEN } from "../../../utils/config";
export default function PopupFrom() {
  const { mess, locationSelected } = useSelector(
    (state) => state.listLocationSlice
  );
  const { tokenUser } = useSelector((state) => state.userSlice);
  const dispatch = useDispatch();
  const [local, setLocal] = useState({
    tenViTri: "",
    tinhThanh: "",
    hinhAnh: "",
    quocGia: "",
  });
  useEffect(() => {
    if (!mess && locationSelected) {
      setLocal(locationSelected);
    }
  }, []);
  const handleOnchange = (event) => {
    let { value, name } = event.target;
    setLocal({ ...local, [name]: value });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    if (mess) {
      https
        .post("/api/vi-tri", local, {
          headers: {
            token: tokenUser,
            tokenCybersoft: CYBER_TOKEN,
          },
        })
        .then((res) => {
          message.success("Thêm vị trí thành công");
          setLocal({
            tenViTri: "",
            tinhThanh: "",
            hinhAnh: "",
            quocGia: "",
          });
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    } else {
      https
        .put(`/api/vi-tri/${locationSelected.id}`, local, {
          headers: {
            token: tokenUser,
            tokenCybersoft: CYBER_TOKEN,
          },
        })
        .then((res) => {
          message.success("Cập nhập vị trí thành công");
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    }
  };
  return (
    <>
      <Popup
        content={
          <>
            <h2 className="font-medium md:text-2xl  text-xl text-purple-400">
              {mess ? "Thêm vị trí" : "Chỉnh sử thông tin"}
            </h2>
            <hr />
            <div>
              <form onSubmit={handleSubmit}>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="tenViTri"
                    id="tenViTri"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={local.tenViTri}
                  />
                  <label
                    htmlFor="tenViTri"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Tên vị trí
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="tinhThanh"
                    id="tinhThanh"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={local.tinhThanh}
                  />
                  <label
                    htmlFor="tinhThanh"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Tỉnh thành
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="quocGia"
                    id="quocGia"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={local.quocGia}
                  />
                  <label
                    htmlFor="quocGia"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Quốc gia
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="hinhAnh"
                    id="hinhAnh"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={local.hinhAnh}
                  />
                  <label
                    htmlFor="hinhAnh"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Link hình ảnh
                  </label>
                </div>
                <div className="flex items-center justify-end space-x-5">
                  <button
                    type="submit"
                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    {mess ? "Submit" : "Update"}
                  </button>
                  <button
                    type="button"
                    className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                    onClick={() => {
                      dispatch(setTogglePopup(false));
                    }}
                  >
                    Hủy
                  </button>
                </div>
              </form>
            </div>
          </>
        }
      />
    </>
  );
}
