import React from "react";
import Popup from "../../../Components/Popup/Popup";
import { setTogglePopupDetail } from "../../../redux/listLocationSlice";
import { useDispatch, useSelector } from "react-redux";

export default function PopupDetail() {
  const dispatch = useDispatch();
  const { locationSelected } = useSelector((state) => state.listLocationSlice);
  let { id, tenViTri, tinhThanh, quocGia, hinhAnh } = locationSelected || {};
  return (
    <>
      <Popup
        content={
          <>
            <h2 className="font-medium md:text-2xl  text-xl text-purple-400 ">
              Thông tin chi tiết
            </h2>
            <hr className="my-3" />
            <div >
              {hinhAnh ? (
                <>
                  <div className="md:flex items-center justify-evenly space-x-2">
                    <div className="md:w-3/12 w-1/2">
                      <img src={hinhAnh} alt="Hinh anh"  className=" w-full h-auto object-cover"/>
                    </div>
                    <div className="md:w-9/12 w-full">
                      <div className="font-medium flex items-center justify-between">
                        <h5>Mã:</h5>
                        <span className="text-blue-500">{id}</span>
                      </div>
                      <div className="font-medium flex items-center justify-between">
                        <h5>Tên Vị trí:</h5>
                        <span className="text-blue-500">{tenViTri}</span>
                      </div>
                      <div className="font-medium flex items-center justify-between">
                        <h5>Tỉnh thành:</h5>
                        <span className="text-blue-500">{tinhThanh}</span>
                      </div>
                      <div className="font-medium flex items-center justify-between">
                        <h5>Quốc gia :</h5>
                        <span className="text-blue-500">{quocGia}</span>
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Mã:</h5>
                    <span className="text-blue-500">{id}</span>
                  </div>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Tên Vị trí:</h5>
                    <span className="text-blue-500">{tenViTri}</span>
                  </div>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Hình Ảnh :</h5>
                    <span className="text-blue-500">-</span>
                  </div>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Tỉnh thành:</h5>
                    <span className="text-blue-500">{tinhThanh}</span>
                  </div>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Quốc gia :</h5>
                    <span className="text-blue-500">{quocGia}</span>
                  </div>
                </>
              )}
            </div>
            <hr className="my-3" />
            <div className="flex justify-end">
              <button
                type="button"
                className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                onClick={() => {
                  dispatch(setTogglePopupDetail(false));
                }}
              >
                Đóng
              </button>
            </div>
          </>
        }
      />
    </>
  );
}
