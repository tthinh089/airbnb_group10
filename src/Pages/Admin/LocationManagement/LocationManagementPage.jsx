import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../responsives/responsive'
import LocationManagementPageDesktop from './LocationManagementPageDesktop'
import LocationManagementPageTablet from './LocationManagementPageTablet'
import LocationManagementPageMobile from './LocationManagementPageMobile'

export default function LocationManagementPage() {
  return (
    <>
      <Desktop>
        <LocationManagementPageDesktop/>
      </Desktop>
      <Tablet>
        <LocationManagementPageTablet/>
      </Tablet>
      <Mobile>
        <LocationManagementPageMobile/>
      </Mobile>
    </>
  )
}
