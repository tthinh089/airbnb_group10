import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setMess, setTogglePopup } from "../../../redux/listLocationSlice";
import PopupFrom from "./PopupForm";
import PopupDetail from "./PopupDetail";
import Pagination from "./Pagination";
import LocationTable from "./LocationTable";
import { https } from "../../../Services/config";
import PopupFileImg from "./PopupFileImg";
import { useSearchParams } from "react-router-dom";

export default function LocationManagementPageDesktop() {
   const [searchParams, setSearchParams] = useSearchParams();
  const paramPageIndex = searchParams.get("pageIndex");
  const paramPageSize = searchParams.get("pageSize") * 1;
  const { togglePopup, togglePopupDetail, listLocation, togglePopupUpload } = useSelector(
    (state) => state.listLocationSlice
  );
  const { userInfo } = useSelector((state) => state.userSlice);
 const [currentPage, setCurrentPage] = useState(paramPageIndex || 1);
  const [recordsPerPage] = useState(paramPageSize || 10);
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  useEffect(() => {
    if (userInfo ) {
       https
      .get(`/api/vi-tri`)
      .then((res) => {
        setData(res.data.content);
      })
      .catch((err) => {
        console.log("err", err);
      });
    }
  }, []);
  let nPages = 1;
  if (listLocation) {
    nPages = Math.ceil(data.length / recordsPerPage);
  }

  return (
    <div
      className="md:ml-64 lg:flex lg:flex-col md:w-75%  "
      style={{ background: `linear-gradient(to right, #ACB6E5, #74ebd5)` }}
    >
      <div className="w-full h-full flex items-center justify-center ">
        <div className="w-11/12 h-4/6 bg-white mt-5 rounded-lg">
          <div className="flex items-center justify-between p-3 border-b-2 border-gray-200">
            <h3 className="text-2xl text-blue-500 font-bold">
              Danh sách vị trí
            </h3>
            <button
              className="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded"
              onClick={() => {
                dispatch(setTogglePopup(true));
                dispatch(setMess(true));
              }}
            >
              Thêm quản vị trí
            </button>
          </div>
          <div>
            <LocationTable
              currentPage={currentPage}
              recordsPerPage={recordsPerPage}
            />
            {togglePopup && <PopupFrom />}
            {togglePopupDetail && <PopupDetail />}
            {togglePopupUpload && <PopupFileImg></PopupFileImg>}
          </div>
        </div>
      </div>
      {/* paging  */}
      <Pagination
        nPages={nPages}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        recordsPerPage={recordsPerPage}
        paramPageIndex= {paramPageIndex}
      />
    </div>
  );
}
