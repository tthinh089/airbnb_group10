import React, { useEffect, useState } from "react";
import Popup from "../../../Components/Popup/Popup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import {  setTogglePopup } from "../../../redux/listUserSlice";
import { https } from "../../../Services/config";
import { message } from "antd";
export default function PopupFrom() {
  const [startDate, setStartDate] = useState(new Date());
  const { mess, uerSelected } = useSelector((state) => state.listUserSlice);
  const dispatch = useDispatch();
  const [admin, setAdmin] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
    birthday: moment(startDate).format("YYYY-MM-DD" || "DD/MM/YYYY"),
    gender: true,
    role: "ADMIN",
  });
  useEffect(() => {
    if (!mess) {
      setStartDate(
        Date.parse(moment(uerSelected.birthday, "YYYY-MM-DD" || "DD/MM/YYYY"))
      );
      setAdmin(uerSelected);
    }
  }, []);
  const handleOnchange = (event) => {
    let { value, name } = event.target;
    setAdmin({ ...admin, [name]: value });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    
      if (mess) {
      https
        .post("/api/users", admin)
        .then((res) => {
          message.success("Thêm quản trị viên thành công");
          setStartDate(new Date());
          setAdmin({
            name: "",
            email: "",
            password: "",
            phone: "",
            birthday: moment(startDate).format("YYYY-MM-DD" || "DD/MM/YYYY"),
            gender: true,
            role: "ADMIN",
          });
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    } else {
      https
        .put(`/api/users/${uerSelected.id}`, admin)
        .then((res) => {
          message.success("Cập nhập quản trị viên thành công");
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
        });
    }
    } 
    
  return (
    <>
      <Popup
        content={
          <>
            <h2 className="font-medium md:text-2xl  text-xl text-purple-400">
              {mess ? "Thêm quản trị viên" : "Chỉnh sử thông tin"}
            </h2>
            <hr />
            <div>
              <form onSubmit={handleSubmit}>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="name"
                    id="name"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={admin.name}
                  />
                  <label
                    htmlFor="name"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Name
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="email"
                    name="email"
                    id="email"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={admin.email}
                  />
                  <label
                    htmlFor="email"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Email address
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="password"
                    name="password"
                    id="password"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={admin.password}
                  />
                  <label
                    htmlFor="password"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Password
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group"></div>
                <div className="relative z-0 w-full mb-6 group">
                  <div className="relative z-0 w-full mb-6 group">
                    <input
                      onChange={handleOnchange}
                      type="number"
                      pattern="[0-9]"
                      name="phone"
                      id="phone"
                      className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                      value={admin.phone}
                    />
                    <label
                      htmlFor="phone"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Phone number
                    </label>
                  </div>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  {!mess ? (
                    <>
                      <label
                        htmlFor="role"
                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                      >
                        Select role
                      </label>
                      <select
                        onChange={handleOnchange}
                        value={admin.role}
                        id="role"
                        name="role"
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      >
                        <option value="ADMIN">ADMIN</option>
                        <option value="USER">USER</option>
                      </select>
                    </>
                  ) : (
                    <></>
                  )}
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <DatePicker
                    required
                    value={startDate}
                    name="birthday"
                    id="birthday"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    selected={startDate}
                    onChange={(date) => {
                      setStartDate(date);
                      setAdmin({
                        ...admin,
                        birthday: moment(date).format(
                          "YYYY-MM-DD" || "DD/MM/YYYY"
                        ),
                      });
                    }}
                  />
                  <label
                    htmlFor="birthday"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Birthday
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="gender"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Select your gender
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={admin.gender}
                    id="gender"
                    name="gender"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Male</option>
                    <option value="false">Female</option>
                  </select>
                </div>
                
                <div className="flex items-center justify-end space-x-5">
                  <button
                    type="submit"
                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    {mess ? "Submit" : "Update"}
                  </button>
                  <button
                    type="button"
                    className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                    onClick={() => {
                      dispatch(setTogglePopup(false));
                    }}
                  >
                    Hủy
                  </button>
                </div>
              </form>
            </div>
          </>
        }
      />
    </>
  );
}
