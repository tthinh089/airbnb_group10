import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../responsives/responsive'
import UserManagementPageDesktop from './UserManagementPageDesktop'
import UserManagementPageTablet from './UserManagementPageTablet'
import UserManagementPageMobile from './UserManagementPageMobile'

export default function UserManagementPage() {
  return (
    <>
      <Desktop>
        <UserManagementPageDesktop/>
      </Desktop>
      <Tablet>
        <UserManagementPageTablet/>
      </Tablet>
      <Mobile>
        <UserManagementPageMobile/>
      </Mobile>
    </>
  )
}
