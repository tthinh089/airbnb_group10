import React, { useState } from "react";
import Popup from "../../../Components/Popup/Popup";
import { setTogglePopupDetail } from "../../../redux/listUserSlice";
import { useDispatch, useSelector } from "react-redux";

export default function PopupDetail() {
  const dispatch = useDispatch();
  const { uerSelected } = useSelector((state) => state.listUserSlice);
  let { birthday, email, gender, id, name, phone, role } = uerSelected;
  return (
    <>
          <Popup
        content={
          <>
            <h2 className="font-medium md:text-2xl  text-xl text-purple-400 ">
              Thông tin chi tiết
            </h2>
            <hr className="my-3" />
            <div className="my-3">
              <div className="font-medium flex items-center justify-between">
                <h5>Id:</h5>
                <span className="text-blue-500">{id}</span>
              </div>
              <div className="font-medium flex items-center justify-between">
                <h5>Name:</h5>
                <span className="text-blue-500">{name}</span>
              </div>
              <div className="font-medium flex items-center justify-between">
                <h5>Gender:</h5>
                <span className="text-blue-500">{gender? "Male" : "Female"}</span>
              </div>
              <div className="font-medium flex items-center justify-between">
                <h5>Birthday :</h5>
                <span className="text-blue-500">{birthday}</span>
              </div>
              <div className="font-medium flex items-center justify-between">
                <h5>Email:</h5>
                <span className="text-blue-500">{email}</span>
              </div>

              <div className="font-medium flex items-center justify-between">
                <h5>Phone:</h5>
                <span className="text-blue-500">{phone}</span>
              </div>
              <div className="font-medium flex items-center justify-between">
                <h5>Role:</h5>
                <span className="text-blue-500">{role}</span>
              </div>
            </div>
            <hr className="my-3" />
            <div className="flex justify-end">
              <button
                type="button"
                className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                onClick={() => {
                  dispatch(setTogglePopupDetail(false));
                }}
              >
                Đóng
              </button>
            </div>
          </>
        }
      />
    </>
  );
}
