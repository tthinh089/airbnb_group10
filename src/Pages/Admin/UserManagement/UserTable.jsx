import React, { useEffect, useState } from "react";
import { https } from "../../../Services/config";
import User from "./User";
import { useDispatch, useSelector } from "react-redux";
import { setListUser } from "../../../redux/listUserSlice";

export default function (props) {
  const { listUser } = useSelector((state) => state.listUserSlice);
  const dispatch = useDispatch();
  let { isSearch, listUserSearch, recordsPerPage, currentPage } = props;

  useEffect(() => {
    if ((currentPage, recordsPerPage)) {
      https
        .get(
          `/api/users/phan-trang-tim-kiem?pageIndex=${currentPage}&pageSize=${recordsPerPage}`
        )
        .then((res) => {
          dispatch(setListUser(res.data.content.data));
        })
        .catch((err) => {
          console.log("err", err);
        });
    }
  }, [currentPage, recordsPerPage]);
  const renderList = (list) => {
    return list.map((user) => {
      return <User key={user.id} user={user} />;
    });
  };
  return (
    <div className=" horizontal-scroll-except-first-column overflow-auto">
      <table  className=" table-auto  border-collapse border border-slate-400 w-full overscroll-auto">
        <thead>
          <tr className="text-purple-500 ">
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">Id</th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">Name</th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">Avatar</th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">Phone</th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">Birthday</th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">Gender</th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">Role</th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">Actions</th>
          </tr>
        </thead>
        <tbody>
          {isSearch ? renderList(listUserSearch) : renderList(listUser)}
        </tbody>
      </table>
    </div>
  );
}
