import React, { useEffect } from "react";
import { useSearchParams } from "react-router-dom";

export default function Pagination({
  nPages,
  currentPage,
  setCurrentPage,
  valueInput,
  recordsPerPage,
}) {
  const [searchParams, setSearchParams] = useSearchParams();
  const pageNumbers = [...Array(nPages + 1).keys()].slice(1);
  const goToNextPage = () => {
    if (currentPage < nPages) setCurrentPage(currentPage + 1);
    };
    
  const goToPrevPage = () => {
    if (currentPage > 1) setCurrentPage(currentPage - 1);
    };
    
  useEffect(() => {
    let searchKey;
    if (currentPage, nPages) {
        searchKey = {
          search: valueInput? valueInput:"",
          pageIndex: currentPage,
          pageSize: recordsPerPage,
        };
      setSearchParams(searchKey, { replace: true });
    }
  }, [currentPage, nPages]);
  return (
    <div className="w-11/12 mt-5 pb-5">
      <nav aria-label="Page navigation example ">
        <ul className="flex items-center justify-end -space-x-px h-10 md:text-base text-sm overflow-auto">
          <li>
            <a
              onClick={goToPrevPage}
              href="#"
              className="flex items-center justify-center md:px-4 px-1 h-10 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-l-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
            >
              <span className="sr-only">Previous</span>
              <svg
                className="w-3 h-3"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 6 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M5 1 1 5l4 4"
                />
              </svg>
            </a>
          </li>
          {pageNumbers.map((itemPg) => {
            return (
              <li 
                key={itemPg}
                onClick={() => {
                  setCurrentPage(itemPg);
                }}
                className={
                  currentPage === itemPg? "bg-purple-300" : "bg-white "
                }
              >
                <a
                  href="#"
                  aria-current="page"
                  className="flex items-center justify-center md:px-4 px-1 h-10 leading-tight text-gray-500 border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                >
                  {itemPg}
                </a>
              </li>
            );
          })}

          <li>
            <a
              onClick={goToNextPage}
              href="#"
              className="flex items-center justify-center md:px-4 px-1 h-10 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
            >
              <span className="sr-only">Next</span>
              <svg
                className="w-3 h-3"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 6 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m1 9 4-4-4-4"
                />
              </svg>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
}
