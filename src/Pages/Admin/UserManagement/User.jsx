import React from "react";
import { FaEye, FaEdit, FaTrashAlt } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { setMess, setTogglePopup, setTogglePopupDetail, setUserSelected } from "../../../redux/listUserSlice";
import { https } from "../../../Services/config";
import { message } from "antd";
export default function User(props) {
   const dispatch = useDispatch();
  let { user } = props;
  let { avatar, birthday, gender, id, name, phone, role } = user;
  const handleDelete = (id) => { 
    https.delete(`/api/users?id=${id}`)
     .then((res) => { 
          message.success("Xóa thông tin thành công")
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => { 
          console.log("err ", err);
          message.error(err.response.data.content)
        
       })
  }

  return (
    <>
      <tr className="text-center md:text-base text-xs">
        <td className="border border-slate-300 md:p-2 ">{id}</td>
        <td className="border border-slate-300 md:p-2 ">{name}</td>
        <td className="border border-slate-300 md:p-2 ">{avatar ? <img src={avatar} alt="avatar" className="lg:w-36  w-full h-auto object-cover" />: "-"}</td>
        <td className="border border-slate-300 md:p-2  truncate">{ phone}</td>
        <td className="border border-slate-300 md:p-2  truncate">{ birthday}</td>
        <td className="border border-slate-300 md:p-2  ">{gender? "Male" : "Female"}</td>
        <td className="border border-slate-300 md:p-2  ">{role}</td>
        <td className="border border-slate-300 md:p-2   space-y-1 ">
          <button className=" flex space-x-1 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-700 rounded" onClick={() => { 
            dispatch(setTogglePopupDetail(true))
            dispatch(setUserSelected(user));
           }}>
            <FaEye /> <span>View Detail</span>
          </button>
          <button className="flex space-x-1 bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 border border-yellow-700 rounded" onClick={() => {
            dispatch(setMess(false));
            dispatch(setTogglePopup(true));
          dispatch(setUserSelected(user));}}>
            <FaEdit />
            Edit
          </button>
          {role !=="ADMIN" && <button className="flex space-x-1 bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-700 rounded" onClick={() => { 
            handleDelete(`${id}`)
           }}>
            <FaTrashAlt />
            <span>Delete</span>
          </button>}
        </td>
      </tr>
    </>
  );
}
