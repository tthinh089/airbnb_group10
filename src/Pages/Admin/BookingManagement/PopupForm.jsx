import React, { useEffect, useState } from "react";
import Popup from "../../../Components/Popup/Popup";
import "react-datepicker/dist/react-datepicker.css";
import { useDispatch, useSelector } from "react-redux";
import { setTogglePopup } from "../../../redux/bookingManagementSlice";
import { https } from "../../../Services/config";
import { message } from "antd";
import moment from "moment";
import DatePicker from "react-datepicker";

export default function PopupFrom() {
  const { mess, bookingSelected } = useSelector(
    (state) => state.bookingManagementSlice
  );
  const { tokenUser } = useSelector((state) => state.userSlice);
  const dispatch = useDispatch();
  const [startDate, setStartDate] = useState(new Date());
  const [endtDate, setEndtDate] = useState(new Date());

  const [ticket, setTicket] = useState({
    maPhong: "",
    ngayDen: startDate,
    ngayDi: endtDate,
    soLuongKhach: "",
    maNguoiDung: "",
  });
  useEffect(() => {
    if (!mess) {
      setStartDate(
        Date.parse(
          moment(bookingSelected.ngayDen, "YYYY-MM-DD" || "DD/MM/YYYY")
        )
      );
      setEndtDate(
        Date.parse(moment(bookingSelected.ngayDi, "YYYY-MM-DD" || "DD/MM/YYYY"))
      );
      setTicket(bookingSelected);
    }
  }, []);
  const handleOnchange = (event) => {
    let { value, name } = event.target;
    setTicket({ ...ticket, [name]: value });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    if (mess) {
      https
        .post("/api/dat-phong", ticket)
        .then((res) => {
          message.success("Thêm vị trí thành công");
          setTicket({
            maPhong: "",
            ngayDen: startDate,
            ngayDi: endtDate,
            soLuongKhach: "",
            maNguoiDung: "",
          });
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    } else {
      https
        .put(`/api/dat-phong/${bookingSelected.id}`, ticket)
        .then((res) => {
          message.success("Cập nhập vị trí thành công");
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    }
  };
  return (
    <>
      <Popup
        content={
          <>
            <h2 className="font-medium md:text-2xl  text-xl text-purple-400">
              {mess ? "Thêm vị trí" : "Chỉnh sử thông tin"}
            </h2>
            <hr />
            <div>
              <form onSubmit={handleSubmit}>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="number"
                    name="maPhong"
                    id="maPhong"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={ticket.maPhong}
                  />
                  <label
                    htmlFor="maPhong"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Mã phòng
                  </label>
                </div>
                <div className="relative  w-full mb-6 group z-10">
                  <DatePicker
                    required
                    value={startDate}
                    name="ngayDen"
                    id="ngayDen"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    selected={startDate}
                    onChange={(date) => {
                      setStartDate(date);
                      setTicket({
                        ...ticket,
                        ngayDen: date,
                      });
                    }}
                  />
                  <label
                    htmlFor="ngayDen"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Ngày đến
                  </label>
                </div>
                <div className="relative  w-full mb-6 group z-10">
                  <DatePicker
                    required
                    value={endtDate}
                    name="ngayDi"
                    id="ngayDi"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    selected={endtDate}
                    onChange={(date) => {
                      setEndtDate(date);
                      setTicket({
                        ...ticket,
                        ngayDi: date,
                      });
                    }}
                  />
                  <label
                    htmlFor="ngayDi"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Ngày đi
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="soLuongKhach"
                    id="soLuongKhach"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={ticket.soLuongKhach}
                  />
                  <label
                    htmlFor="soLuongKhach"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Số lượng khách
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="maNguoiDung"
                    id="maNguoiDung"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={ticket.maNguoiDung}
                  />
                  <label
                    htmlFor="maNguoiDung"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Mã người dùng
                  </label>
                </div>
                <div className="flex items-center justify-end space-x-5">
                  <button
                    type="submit"
                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    {mess ? "Submit" : "Update"}
                  </button>
                  <button
                    type="button"
                    className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                    onClick={() => {
                      dispatch(setTogglePopup(false));
                    }}
                  >
                    Hủy
                  </button>
                </div>
              </form>
            </div>
          </>
        }
      />
    </>
  );
}
