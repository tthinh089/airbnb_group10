import React from "react";
import { FaEye, FaEdit, FaTrashAlt } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import {
  setBookingSelected,
  setMess,
  setTogglePopup,
  setTogglePopupDetail,
} from "../../../redux/bookingManagementSlice";
import { https } from "../../../Services/config";
import { message } from "antd";

export default function Booking(props) {
  const dispatch = useDispatch();
  
  let { ticket } = props;
  let { id, maPhong, ngayDen, ngayDi, soLuongKhach,  maNguoiDung} = ticket;
  const { tokenUser } = useSelector((state) => state.userSlice);

  const handleDelete = (id) => {
    if (tokenUser) {
      https
        .delete(`/api/dat-phong/${id}`, {
        })
        .then((res) => {
          message.success("Xóa thông tin thành công");
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    }
  };

  return (
    <>
      <tr className="text-center md:text-base text-xs">
        <td className="border border-slate-300 md:p-2 ">{id}</td>
        <td className="border border-slate-300 md:p-2 ">{maPhong}</td>

        <td className="border border-slate-300 md:p-2 ">{moment(ngayDen).format("DD-MM-YYYY")}</td>
        <td className="border border-slate-300 md:p-2 ">
          {moment(ngayDi).format("DD-MM-YYYY")}
        </td>
        <td className="border border-slate-300 md:p-2  truncate">{soLuongKhach}</td>
        <td className="border border-slate-300 md:p-2  truncate">{maNguoiDung}</td>
        <td className="border border-slate-300 md:p-2   space-y-1 ">
          <button
            className=" flex space-x-1 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-700 rounded"
            onClick={() => {
              dispatch(setTogglePopupDetail(true));
              dispatch(setBookingSelected(ticket));
            }}
          >
            <FaEye /> <span>View Detail</span>
          </button>
          <button
            className="flex space-x-1 bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 border border-yellow-700 rounded"
            onClick={() => {
              dispatch(setMess(false));
              dispatch(setTogglePopup(true));
              dispatch(setBookingSelected(ticket));
            }}
          >
            <FaEdit />
            Edit
          </button>
          {tokenUser && (
            <button
              className="flex space-x-1 bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-700 rounded"
              onClick={() => {
                handleDelete(`${id}`);
              }}
            >
              <FaTrashAlt />
              <span>Delete</span>
            </button>
          )}
        </td>
      </tr>
    </>
  );
}
