import React, { useEffect } from "react";
import { https } from "../../../Services/config";
import { useDispatch, useSelector } from "react-redux";
import { setListBooking } from "../../../redux/bookingManagementSlice";
import Booking from "./Booking";

export default function BookingTable(props) {
    let {  currentRecords , listBookingSearch, isSearch } = props;
 
  const renderList = (list) => {
    return list?.map((ticket) => {
      return <Booking key={ticket.id} ticket={ticket} />;
    });
  };
  return (
    <div className="horizontal-scroll-except-first-column overflow-auto">
      <table className="table-auto  border-collapse border border-slate-400 w-full overscroll-auto">
        <thead>
          <tr className="text-purple-500 ">
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              ID
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Mã Phòng
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Ngày đến
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Ngày đi
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Số lượng khách
            </th>
             <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
            Mã người dùng
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Actions
            </th>
          </tr>
        </thead>
        <tbody>{isSearch ? renderList(listBookingSearch) : renderList(currentRecords)}</tbody>
      </table>
    </div>
  );
}
