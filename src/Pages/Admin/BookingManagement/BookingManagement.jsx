import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../responsives/responsive'
import BookingManagementDesktop from './BookingManagementDesktop'
import BookingManagementTablet from './BookingManagementTablet'
import BookingManagementMobile from './BookingManagementMobile'


export default function BookingManagement() {
  return (
    <>
      <Desktop>
        <BookingManagementDesktop/>
      </Desktop>
      <Tablet>
        <BookingManagementTablet/>
      </Tablet>
      <Mobile>
        <BookingManagementMobile/>
      </Mobile>
    </>
  )
}
