import React from "react";
import Popup from "../../../Components/Popup/Popup";
import { setTogglePopupDetail } from "../../../redux/bookingManagementSlice";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

export default function PopupDetail() {
  const dispatch = useDispatch();
  const { bookingSelected } = useSelector((state) => state.bookingManagementSlice);
  let { id, maPhong, ngayDen, ngayDi, soLuongKhach,  maNguoiDung} = bookingSelected || {};
  return (
    <>
      <Popup
        content={
          <>
            <h2 className="font-medium md:text-2xl  text-xl text-purple-400 ">
              Thông tin chi tiết
            </h2>
            <hr className="my-3" />
            <div >

                  <div className="font-medium flex items-center justify-between">
                    <h5>Mã:</h5>
                    <span className="text-blue-500">{id}</span>
                  </div>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Mã phòng:</h5>
                    <span className="text-blue-500">{maPhong}</span>
                  </div>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Ngày đến :</h5>
                <span className="text-blue-500">{moment(ngayDen).format("DD-MM-YYYY")}</span>
                  </div>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Ngày đi:</h5>
                    <span className="text-blue-500">{moment(ngayDi).format("DD-MM-YYYY")}</span>
                  </div>
                  <div className="font-medium flex items-center justify-between">
                    <h5>Số lượng khách hàng :</h5>
                    <span className="text-blue-500">{soLuongKhach}</span>
              </div>
              <div className="font-medium flex items-center justify-between">
                    <h5>Mã người dùng :</h5>
                    <span className="text-blue-500">{maNguoiDung}</span>
                  </div>
            </div>
            <hr className="my-3" />
            <div className="flex justify-end">
              <button
                type="button"
                className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                onClick={() => {
                  dispatch(setTogglePopupDetail(false));
                }}
              >
                Đóng
              </button>
            </div>
          </>
        }
      />
    </>
  );
}
