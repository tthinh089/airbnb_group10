import React from "react";
import Popup from "../../../Components/Popup/Popup";
import { setTogglePopupDetail } from "../../../redux/roomManagementSlice";
import { useDispatch, useSelector } from "react-redux";
import {FaCheckCircle, FaTimesCircle } from "react-icons/fa";
export default function PopupDetail() {
  const dispatch = useDispatch();
  const { roomSelected, localSelect } = useSelector(
    (state) => state.roomManagementSlice
  );
  let {
    id,
    tenPhong,
    khach,
    phongNgu,
    giuong,
    phongTam,
    moTa,
    giaTien,
    mayGiat,
    banLa,
    tivi,
    dieuHoa,
    wifi,
    bep,
    doXe,
    hoBoi,
    banUi,
    maViTri,
    hinhAnh,
  } = roomSelected || {};
  return (
    <>
      <Popup
        content={
          <>
            <h2 className="font-medium md:text-2xl  text-xl text-purple-400 ">
              Thông tin chi tiết
            </h2>
            <hr className="my-3" />
            <div>
              <div className=" space-x-2">
                <div className="w-2/3">
                  <img
                    src={hinhAnh}
                    alt="Hinh anh"
                    className=" w-full h-auto object-cover"
                  />
                </div>
              </div>

              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Mã:</h5>
                <span className="text-blue-500">{id}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Tên phòng:</h5>
                <span className="text-blue-500">{tenPhong}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Mã vị trí</h5>
                <span className="text-blue-500">{maViTri}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>vị Trí</h5>
                <span className="text-blue-500">
                  {localSelect ? localSelect : "-"}
                </span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Khách tối đa:</h5>
                <span className="text-blue-500">{khach}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Mô tả:</h5>
                <span className="text-blue-500 truncate">{moTa}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Gia tiên</h5>
                <span className="text-blue-500 truncate">{giaTien}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Máy giặc</h5>
                <span className="text-blue-500 truncate">{mayGiat? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
               <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Bàn là</h5>
                <span className="text-blue-500 truncate">{banLa? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
               <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Tivi</h5>
                <span className="text-blue-500 truncate">{tivi? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Điều hòa</h5>
                <span className="text-blue-500 truncate">{dieuHoa? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Wifi</h5>
                <span className="text-blue-500 truncate">{wifi? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Bếp</h5>
                <span className="text-blue-500 truncate">{bep? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Đỗ xe</h5>
                <span className="text-blue-500 truncate">{doXe? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Hồ bơi</h5>
                <span className="text-blue-500 truncate">{hoBoi? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
              <div className="font-medium flex items-center justify-between  space-x-3">
                <h5>Bàn ủi</h5>
                <span className="text-blue-500 truncate">{banUi? <FaCheckCircle className="text-green-700"/>: <FaTimesCircle className="text-red-600"/>}</span>
              </div>
              
            </div>
            <hr className="my-3" />
            <div className="flex justify-end">
              <button
                type="button"
                className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                onClick={() => {
                  dispatch(setTogglePopupDetail(false));
                }}
              >
                Đóng
              </button>
            </div>
          </>
        }
      />
    </>
  );
}
