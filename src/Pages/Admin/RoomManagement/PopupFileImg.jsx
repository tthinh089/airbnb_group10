import React, { useState } from "react";
import Popup from "../../../Components/Popup/Popup";
import {
  setTogglePopupUpload,
} from "../../../redux/roomManagementSlice";
import { CYBER_TOKEN } from "../../../utils/config";
import { https } from "../../../Services/config";
import { useDispatch, useSelector } from "react-redux";
import { message } from "antd";
export default function PopupFileImg() {
  const { roomSelected } = useSelector((state) => state.roomManagementSlice);
  const { tokenUser } = useSelector((state) => state.userSlice);
  const [hinhAnh, setHinhAnh] = useState(roomSelected?.hinhAnh || "");
  const dispatch = useDispatch();
  const handleUploadImg = (e) => {
    e.preventDefault();
    if ((tokenUser, roomSelected)) {
      
      if (!hinhAnh) {
        message.error("No file selected");
        return;
      }
      var formData = new FormData();
      formData.append("formFile", hinhAnh);
      https
        .post(
          `/api/phong-thue/upload-hinh-phong?maPhong=${roomSelected.id}`,
          formData,
          {
            headers: {
              token: tokenUser,
              tokenCybersoft: CYBER_TOKEN,
            },
          }
        )
        .then((res) => {
          message.success("Upload hình thành công ");
          setTimeout(() => {
            window.location.reload();
          }, 2000);

        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    }
  };
  const handleOnchange = (event) => { 
    const selectedFile = event.target.files[0];
    setHinhAnh(selectedFile);
   }
  return (
    <Popup
      content={
        <>
          <h2 className="font-medium text-2xl text-purple-400 ">
            Upload hình phòng thuê
          </h2>
          <hr className="my-3" />
          <div className="my-3">
            <form onSubmit={handleUploadImg}>
              <div className="relative z-0 w-full mb-6 group">
                <input
                  onChange={handleOnchange}
                  type="file"
                  name="hinhAnh"
                  id="hinhAnh"
                  className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  required
                  accept="image/png, image/gif, image/jpeg"
                />
                <label
                  htmlFor="hinhAnh"
                  className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Upload hình ảnh
                </label>
              </div>
              <div className="flex justify-end space-x-2">
                <button
                  type="submit"
                  className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Update Img
                </button>
                <button
                  type="button"
                  className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                  onClick={() => {
                    dispatch(setTogglePopupUpload(false));
                  }}
                >
                  Đóng
                </button>
              </div>
            </form>
          </div>
        </>
      }
    ></Popup>
  );
}
