import React, { useEffect, useState } from "react";
import { FaEye, FaEdit, FaTrashAlt } from "react-icons/fa";
import { FcStatistics } from "react-icons/fc";
import { useDispatch, useSelector } from "react-redux";
import { https } from "../../../Services/config";
import { Modal, message } from "antd";
import { CYBER_TOKEN } from "../../../utils/config";
import {
  setRoomSelected,
  setMess,
  setTogglePopup,
  setTogglePopupDetail,
  setTogglePopupUpload,
  setLocalSelect,
} from "../../../redux/roomManagementSlice";

export default function Room(props) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const { userInfo } = useSelector((state) => state.userSlice);
  let { room } = props;
  let { id, tenPhong, khach, maViTri, hinhAnh } = room;
  const dispatch = useDispatch();
  const { tokenUser } = useSelector((state) => state.userSlice);
  const [localArr, setLocalArr] = useState([]);
  useEffect(() => {
    if (userInfo) {
      https
        .get(`/api/vi-tri`)
        .then((res) => {
          setLocalArr(res.data.content);
        })
        .catch((err) => {
          console.log("err", err);
        });
    }
  }, []);

  const handleDelete = (id) => {
    if (tokenUser) {
      https
        .delete(`/api/phong-thue/${id}`, {
          headers: {
            token: tokenUser,
            tokenCybersoft: CYBER_TOKEN,
          },
        })
        .then((res) => {
          message.success("Xóa thông tin thành công");
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.message);
        });
    }
  };

  let tenVitri = "-";
  const showViTri = () => {
    localArr?.forEach((local) => {
      if (local.id == maViTri) {
        return (tenVitri = local.tenViTri);
      }
    });
    return tenVitri;
  };

  return (
    <>
      <tr className="text-center md:text-base text-xs">
        <td className="border border-slate-300 md:p-2 ">{id}</td>
        <td className="border border-slate-300 md:p-2 ">{tenPhong}</td>
        <td className="border border-slate-300 md:p-2 ">
          {hinhAnh ? (
            <div className="space-y-2">
              <div className="">
                <img
                  src={hinhAnh}
                  alt="Hình ảnh"
                  className="w-36 h-auto object-cover"
                />
              </div>
              <div className="">
                <button
                  className="bg-purple-300 hover:bg-purple-600 text-white py-1 px-2 rounded-sm"
                  onClick={() => {
                    dispatch(setTogglePopupUpload(true));
                    dispatch(setRoomSelected(room));
                  }}
                >
                  {" "}
                  <FaEdit />
                </button>
              </div>
            </div>
          ) : (
            "-"
          )}
        </td>
        <td className="border border-slate-300 md:p-2 ">{showViTri()}</td>
        <td className="border border-slate-300 md:p-2 ">{maViTri}</td>
        <td className="border border-slate-300 md:p-2 ">{khach}</td>
        <td className="border border-slate-300 md:p-2 space-y-1 w-1/6">
          <button
            className=" flex space-x-1 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-700 rounded"
            onClick={() => {
              dispatch(setTogglePopupDetail(true));
              dispatch(setRoomSelected(room));
              dispatch(setLocalSelect(showViTri()));
            }}
          >
            <FaEye /> <span className="text-sm">View Detail</span>
          </button>
          <button
            className="flex space-x-1 bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 border border-yellow-700 rounded"
            onClick={() => {
              dispatch(setMess(false));
              dispatch(setTogglePopup(true));
              dispatch(setRoomSelected(room));
            }}
          >
            <FaEdit />
            Edit
          </button>
          {tokenUser && (
            <button
              className="flex space-x-1 bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-700 rounded"
              onClick={() => {
                handleDelete(`${id}`);
              }}
            >
              <FaTrashAlt />
              <span>Delete</span>
            </button>
          )}
          <button
            className="flex space-x-1 bg-green-700 hover:bg-green-800 text-white font-bold py-1 px-2 border border-yellow-700 rounded"
            onClick={() => {
              showModal();
            }}
          >
            <FcStatistics />
            Statistic
          </button>
        </td>
      </tr>
      <Modal
        title="Basic Modal"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
    </>
  );
}
