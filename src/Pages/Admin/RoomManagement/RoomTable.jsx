import React, { useEffect } from "react";
import { https } from "../../../Services/config";
import Room from "./Room";
import { useDispatch, useSelector } from "react-redux";
import { setListRoom } from "../../../redux/roomManagementSlice";

export default function RoomTable(props) {
  const { listRoom } = useSelector((state) => state.roomManagementSlice);
  const dispatch = useDispatch();
  let { recordsPerPage, currentPage, isSearch, listRoomSearch } = props;

  useEffect(() => {
    if ((currentPage, recordsPerPage, listRoom)) {
      https
        .get(
          `/api/phong-thue/phan-trang-tim-kiem?pageIndex=${currentPage}&pageSize=${recordsPerPage}`
        )
        .then((res) => {
          dispatch(setListRoom(res.data.content.data));
        })
        .catch((err) => {
          console.log("err", err);
        });
    }
  }, [currentPage, recordsPerPage]);
  const renderList = (list) => {
    return list.map((room) => {
      return <Room key={room.id} room={room} />;
    });
  };

  return (
    <div className="horizontal-scroll-except-first-column overflow-auto">
      <table className="table-auto  border-collapse border border-slate-400 w-full overscroll-auto">
        <thead>
          <tr className="text-purple-500 ">
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Mã Phòng
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Tên Phòng
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Hình Ảnh
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Vị Trí
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Mã Vị Trí
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Khách tối đa
            </th>
            <th className="border border-slate-300 px-4 py-2 md:text-base text-sm ">
              Actions
            </th>
          </tr>
        </thead>
        <tbody>
          {isSearch ? renderList(listRoomSearch) : renderList(listRoom)}
        </tbody>
      </table>
    </div>
  );
}
