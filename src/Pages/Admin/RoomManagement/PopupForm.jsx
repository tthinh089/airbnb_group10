import React, { useEffect, useState } from "react";
import Popup from "../../../Components/Popup/Popup";
import { useDispatch, useSelector } from "react-redux";
import { setTogglePopup } from "../../../redux/roomManagementSlice";
import { https } from "../../../Services/config";
import { message } from "antd";
import { CYBER_TOKEN } from "../../../utils/config";
export default function PopupFrom() {
  const { mess, roomSelected } = useSelector(
    (state) => state.roomManagementSlice
  );
  const { tokenUser } = useSelector((state) => state.userSlice);
  const dispatch = useDispatch();
  const [room, setRoom] = useState({
    tenPhong: "",
    khach: 0,
    phongNgu: 0,
    giuong: 0,
    phongTam: 0,
    moTa: "",
    giaTien: 0,
    mayGiat: false,
    banLa: false,
    tivi: false,
    dieuHoa: false,
    wifi: false,
    bep: false,
    doXe: false,
    hoBoi: false,
    banUi: false,
    maViTri: 0,
    hinhAnh: "",
  });
  useEffect(() => {
    if (!mess && roomSelected) {
      setRoom(roomSelected);
    }
  }, []);
  const handleOnchange = (event) => {
    let { value, name } = event.target;
    setRoom({ ...room, [name]: value });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    if (mess) {
      https
        .post("/api/phong-thue", room, {
          headers: {
            token: tokenUser,
            tokenCybersoft: CYBER_TOKEN,
          },
        })
        .then((res) => {
          message.success("Thêm phòng thuê thành công");
          setRoom({
            tenPhong: "",
            khach: 0,
            phongNgu: 0,
            giuong: 0,
            phongTam: 0,
            moTa: "",
            giaTien: 0,
            mayGiat: false,
            banLa: false,
            tivi: false,
            dieuHoa: false,
            wifi: false,
            bep: false,
            doXe: false,
            hoBoi: false,
            banUi: false,
            maViTri: 0,
            hinhAnh: "",
          });
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    } else {
      https
        .put(`/api/phong-thue/${roomSelected.id}`, room, {
          headers: {
            token: tokenUser,
            tokenCybersoft: CYBER_TOKEN,
          },
        })
        .then((res) => {
          message.success("Cập nhập phòng thuê thành công");
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        })
        .catch((err) => {
          console.log("err ", err);
          message.error(err.response.data.content);
        });
    }
  };
  return (
    <>
      <Popup
        content={
          <>
            <h2 className="font-medium md:text-2xl  text-xl text-purple-400">
              {mess ? "Thêm phòng thuê" : "Chỉnh sử thông tin"}
            </h2>
            <hr className="py-2"/>
            <div>
              <form onSubmit={handleSubmit}>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="tenPhong"
                    id="tenPhong"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.tenPhong}
                  />
                  <label
                    htmlFor="tenPhong"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Tên phòng
                  </label>
                </div>
          <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="number"
                    name="khach"
                    id="khach"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.khach}
                  />
                  <label
                    htmlFor="khach"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Khách
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="number"
                    name="phongNgu"
                    id="phongNgu"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.phongNgu}
                  />
                  <label
                    htmlFor="phongNgu"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Phòng Ngủ
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="number"
                    name="giuong"
                    id="giuong"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.giuong}
                  />
                  <label
                    htmlFor="giuong"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Giường
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="number"
                    name="phongTam"
                    id="phongTam"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.phongTam}
                  />
                  <label
                    htmlFor="phongTam"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Phòng tắm 
                  </label>
                </div>

                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="moTa"
                    id="moTa"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.moTa}
                  />
                  <label
                    htmlFor="moTa"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Mô tả
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="number"
                    name="giaTien"
                    id="giaTien"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.giaTien}
                  />
                  <label
                    htmlFor="giaTien"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Gia tiên
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="mayGiat"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Máy giặt
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.mayGiat}
                    id="mayGiat"
                    name="mayGiat"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="banLa"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Bàn Là
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.banLa}
                    id="banLa"
                    name="banLa"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="tivi"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Tivi
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.tivi}
                    id="tivi"
                    name="tivi"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="dieuHoa"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Điều hòa
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.dieuHoa}
                    id="dieuHoa"
                    name="dieuHoa"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="wifi"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Wifi
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.wifi}
                    id="wifi"
                    name="wifi"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="bep"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Bếp
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.bep}
                    id="bep"
                    name="bep"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="doXe"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Đỗ xe
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.doXe}
                    id="doXe"
                    name="doXe"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="hoBoi"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Hồ bơi
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.hoBoi}
                    id="hoBoi"
                    name="hoBoi"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <label
                    htmlFor="banUi"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Bàn ủi
                  </label>
                  <select
                    onChange={handleOnchange}
                    value={room.banUi}
                    id="banUi"
                    name="banUi"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option value="true">Có</option>
                    <option value="false">Không</option>
                  </select>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="number"
                    name="maViTri"
                    id="maViTri"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.maViTri}
                  />
                  <label
                    htmlFor="maViTri"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Mã vị trí
                  </label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                  <input
                    onChange={handleOnchange}
                    type="text"
                    name="hinhAnh"
                    id="hinhAnh"
                    className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    placeholder=" "
                    required
                    value={room.hinhAnh}
                  />
                  <label
                    htmlFor="hinhAnh"
                    className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                  >
                    Link hình ảnh
                  </label>
                </div>

                <div className="flex items-center justify-end space-x-5">
                  <button
                    type="submit"
                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    {mess ? "Submit" : "Update"}
                  </button>
                  <button
                    type="button"
                    className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                    onClick={() => {
                      dispatch(setTogglePopup(false));
                    }}
                  >
                    Hủy
                  </button>
                </div>
              </form>
            </div>
          </>
        }
      />
    </>
  );
}
