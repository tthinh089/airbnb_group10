import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../responsives/responsive'
import RoomManagementDesktop from './RoomManagementDesktop'
import RoomManagementTablet from './RoomManagementTablet'
import RoomManagementMobile from './RoomManagementMobile'

export default function RoomManagement() {
  return (
        <>
      <Desktop>
        <RoomManagementDesktop/>
      </Desktop>
      <Tablet>
        <RoomManagementTablet/>
      </Tablet>
      <Mobile>
        <RoomManagementMobile/>
      </Mobile>
    </>
  )
}
