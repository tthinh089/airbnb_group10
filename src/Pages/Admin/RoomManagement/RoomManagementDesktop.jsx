import React, { useEffect, useState } from "react";
import RoomTable from "./RoomTable";
import { useDispatch, useSelector } from "react-redux";
import { setMess, setTogglePopup } from "../../../redux/roomManagementSlice";
import PopupFrom from "./PopupForm";
import { FaSearch } from "react-icons/fa";
import PopupDetail from "./PopupDetail";
import { https } from "../../../Services/config";
import { useSearchParams } from "react-router-dom";
import Pagination from "./Pagination";
import PopupFileImg from "./PopupFileImg";

export default function RoomManagementDesktop() {
  const [searchParams, setSearchParams] = useSearchParams();
  const paramPageIndex = searchParams.get("pageIndex");
  const paramPageSize = searchParams.get("pageSize") * 1;
  const paramSearch = searchParams.get("search");
  const [data, setData] = useState([]);
  const [key, setKey] = useState(paramSearch || "");
  const [listRoomSearch, setListRoomSearch] = useState([]);
  const [isSearch, setIsSearch] = useState(false);
  const { togglePopup, togglePopupDetail, togglePopupUpload } = useSelector(
    (state) => state.roomManagementSlice
  );
  const { userInfo } = useSelector((state) => state.userSlice);
  const [currentPage, setCurrentPage] = useState(paramPageIndex || 1);
  const [recordsPerPage] = useState(paramPageSize || 10);
  const dispatch = useDispatch();
  useEffect(() => {
    if (userInfo) {
      https
        .get(`/api/phong-thue`)
        .then((res) => {
          setData(res.data.content);
        })
        .catch((err) => {
          console.log("err", err);
        });
      handleSearch();
    }
  }, []);

  function toNonAccentVietnamese(str) {
    str = str.replace(/A|Á|À|Ã|Ạ|Â|Ấ|Ầ|Ẫ|Ậ|Ă|Ắ|Ằ|Ẵ|Ặ/g, "A");
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/E|É|È|Ẽ|Ẹ|Ê|Ế|Ề|Ễ|Ệ/, "E");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/I|Í|Ì|Ĩ|Ị/g, "I");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/O|Ó|Ò|Õ|Ọ|Ô|Ố|Ồ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ỡ|Ợ/g, "O");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/U|Ú|Ù|Ũ|Ụ|Ư|Ứ|Ừ|Ữ|Ự/g, "U");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/Y|Ý|Ỳ|Ỹ|Ỵ/g, "Y");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/Đ/g, "D");
    str = str.replace(/đ/g, "d");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
    return str;
  }

  const handleSearch = () => {
    if (key.trim() !== "") {
      setIsSearch(true);
      let string = toNonAccentVietnamese(key);
      https
        .get(`/api/phong-thue/lay-phong-theo-vi-tri?maViTri=${string}`)
        .then((res) => {
          setListRoomSearch(res.data.content);
          setCurrentPage(1);
          setIsSearch(true);
        })
        .catch((err) => {
          setIsSearch(false);
          setListRoomSearch([]);
          console.log(err);
        });
    } else {
      setListRoomSearch([]);
      setIsSearch(false);
    }
  };
  const handleOnchange = (event) => {
    let searchKey;
    let { value } = event.target;
    if (value.trim() !== "") {
      searchKey = {
        search: value,
      };
    }
    setSearchParams(searchKey, { replace: true });
    setKey(value);
  };

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
  const currentRecords = listRoomSearch?.slice(
    indexOfFirstRecord,
    indexOfLastRecord
  );
  let nPages = 1;

  if (isSearch) {
    nPages = Math.ceil(listRoomSearch.length / recordsPerPage);
  } else {
    nPages = Math.ceil(data.length / recordsPerPage);
  }

  return (
    <div
      className="md:ml-64 lg:flex lg:flex-col md:w-75%  "
      style={{ background: `linear-gradient(to right, #ACB6E5, #74ebd5)` }}
    >
      {/* Search */}
      <div className=" w-11/12 flex items-center justify-center p-1 mb-4">
        <input
          type="text"
          placeholder="Tìm kiếm phòng theo mã vị trí..."
          className="ml-3 focus:outline-none w-10/12 bg-white  rounded-full border-none  shadow-md p-4"
          value={key}
          onChange={handleOnchange}
        />
        <button
          className="h-auto w-auto bg-purple-700 rounded-md p-4 text-white ml-4"
          onClick={handleSearch}
        >
          <FaSearch />
        </button>
      </div>
      <div className="w-full h-full flex items-center justify-center ">
        <div className="w-11/12 h-4/6 bg-white mt-5 rounded-lg">
          <div className="flex items-center justify-between p-3 border-b-2 border-gray-200">
            <h3 className="text-2xl text-blue-500 font-bold">
              Danh sách phòng
            </h3>
            <button
              className="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded"
              onClick={() => {
                dispatch(setTogglePopup(true));
                dispatch(setMess(true));
              }}
            >
              Thêm phòng
            </button>
          </div>
          <div>
            <RoomTable
              isSearch={isSearch}
              listRoomSearch={currentRecords}
              currentPage={currentPage}
              recordsPerPage={recordsPerPage}
            />
            {togglePopup && <PopupFrom />}
            {togglePopupDetail && <PopupDetail />}
            {togglePopupUpload && <PopupFileImg />}
          </div>
        </div>
      </div>
      {/* paging  */}
      <Pagination
        nPages={nPages}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        recordsPerPage={recordsPerPage}
        valueInput={paramSearch || key}
      />
    </div>
  );
}
