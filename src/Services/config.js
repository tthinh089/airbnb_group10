import axios from "axios";
import { CYBER_TOKEN } from "../utils/config";

export const https = axios.create({
  baseURL: "https://airbnbnew.cybersoft.edu.vn",
  headers: {
    tokenCybersoft: CYBER_TOKEN,
  },
});
