export let localServ = {
  setUser: (user) => {
    let dataJSON = JSON.stringify(user);
    localStorage.setItem("DATA_LOCAL", dataJSON);
  },
  getUser: () => {
    let dataJSON = localStorage.getItem("DATA_LOCAL");
    return JSON.parse(dataJSON);
  },
  removeUser: () => {
    localStorage.removeItem("DATA_LOCAL");
  },
  setInfo: (headerInfo) => {
    let dataJSON = JSON.stringify(headerInfo);
    localStorage.setItem("HEADER_INFO", dataJSON);
  },
  getInfo: () => {
    let dataJSON = localStorage.getItem("HEADER_INFO");
    return JSON.parse(dataJSON);
  },
  setTokenUser: (token) => {
    const dataJson = JSON.stringify(token);
    localStorage.setItem("DATA_TOKEN_USER_LOCAL", dataJson);
  },
  getTokenUser: () => {
    let dataJson = localStorage.getItem("DATA_TOKEN_USER_LOCAL");
    return JSON.parse(dataJson);
  },
};
