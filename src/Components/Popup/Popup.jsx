import React from "react";

export default function Popup(props) {
  return (
    <div className="popup-box z-10 ">
      <div className="box lg:w-1/2 md:w-2/3 w-4/5">
        {props.content}
      </div>
    </div>
  );
}
