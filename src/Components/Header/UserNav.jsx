import React from "react";
import { GlobalOutlined, MenuOutlined, UserOutlined } from "@ant-design/icons";
import { Avatar, Dropdown } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { localServ } from "../../Services/localServ";
import { Desktop, Mobile, Tablet } from "../../responsive/responsiveUserPage";
import UserNavMobile from "./UserNavMobile";

export default function UserNav() {
  let user = useSelector((state) => state.userSlice.userInfo);
  let navigate = useNavigate();
  let handleLogOut = () => {
    localServ.removeUser();
    navigate("/");
    window.location.reload();
  };
  const items =
    user.length === 0
      ? [
          {
            key: "1",
            label: <NavLink to="/register">Đăng ký</NavLink>,
          },
          {
            key: "2",
            label: <NavLink to="/login">Đăng nhập</NavLink>,
          },
          {
            key: "divider-1",
            label: <hr className="my-2 border-gray-300" />,
          },
          {
            key: "3",
            label: (
              <a
                onClick={() => {
                  window.location.href = "https://www.airbnb.com/host/homes";
                }}
              >
                Đón tiếp khách
              </a>
            ),
          },
          {
            key: "4",
            label: (
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.airbnb.com/help?audience=guest"
              >
                Trung tâm hỗ trợ
              </a>
            ),
          },
        ]
      : [
          {
            key: "1",

            label: (
              <NavLink to="/account" className="font-bold">
                Thông tin tài khoản
              </NavLink>
            ),
          },
          user.role === "ADMIN"
            ? {
                key: "2",
                label: <NavLink to="/admin">Truy cập trang ADMIN</NavLink>,
              }
            : null,
          {
            key: "3",
            label: <a onClick={handleLogOut}>Đăng xuất</a>,
          },
          {
            key: "divider-1",
            label: <hr className="my-2 border-gray-300" />,
          },
          {
            key: "4",
            label: (
              <a
                onClick={() => {
                  window.location.href = "https://www.airbnb.com/host/homes";
                }}
              >
                Đón tiếp khách
              </a>
            ),
          },
          {
            key: "5",
            label: (
              <a target="_blank" rel="noopener noreferrer" href="/">
                Trung tâm hỗ trợ
              </a>
            ),
          },
        ];
  return (
    <>
      <Desktop>
        <div className="flex items-start space-x-2 w-80 justify-center">
          <button
            className="font-bold text-gray-800 hover:bg-gray-100 py-5 px-2 => { second } rounded-full"
            onClick={() => {
              window.location.href = "https://www.airbnb.com/host/homes";
            }}
          >
            Đón tiếp khách
          </button>
          <button className="flex items-center hover:bg-gray-100 py-5 px-5 rounded-full">
            <GlobalOutlined className="text-xl leading-3" />
          </button>
          <Dropdown
            menu={{
              items,
            }}
            trigger={["click"]}
            placement="bottomRight"
            overlayStyle={{ width: "240px" }}
          >
            <button className="flex items-center space-x-4 justify-around p-2 border-2 rounded-full text-sm hover:shadow-md transition duration-300">
              <MenuOutlined />
              {user.length != 0 && user.avatar !== "" ? (
                <img src={user.avatar} className="rounded-full w-10 border-2" />
              ) : (
                <Avatar
                  size="large"
                  className="flex items-center justify-center"
                  icon={<UserOutlined />}
                />
              )}
            </button>
          </Dropdown>
        </div>
      </Desktop>
      <Tablet>
        <div className="flex items-start space-x-2 w-80 justify-center">
          <button
            className="font-bold text-gray-800 hover:bg-gray-100 py-5 px-2 => { second } rounded-full"
            onClick={() => {
              window.location.href = "https://www.airbnb.com/host/homes";
            }}
          >
            Đón tiếp khách
          </button>
          <button className="flex items-center hover:bg-gray-100 py-5 px-5 rounded-full">
            <GlobalOutlined className="text-xl leading-3" />
          </button>
          <Dropdown
            menu={{
              items,
            }}
            trigger={["click"]}
            placement="bottomRight"
            overlayStyle={{ width: "240px" }}
          >
            <button className="flex items-center space-x-4 justify-around p-2 border-2 rounded-full text-sm hover:shadow-md transition duration-300">
              <MenuOutlined />
              {user && user.avatar !== "" ? (
                <img src={user.avatar} className="rounded-full w-10 border-2" />
              ) : (
                <Avatar
                  size="large"
                  className="flex items-center justify-center"
                  icon={<UserOutlined />}
                />
              )}
            </button>
          </Dropdown>
        </div>
      </Tablet>
      <Mobile>
        <UserNavMobile />
      </Mobile>
    </>
  );
}
