import React, { useEffect, useState } from "react";
import logo from "./logo.png";
import {
  SearchOutlined,
  UsergroupAddOutlined,
  ArrowUpOutlined,
} from "@ant-design/icons";
import {
  AutoComplete,
  DatePicker,
  Modal,
  Tabs,
  message,
  InputNumber,
  Typography,
} from "antd";
import UserNav from "./UserNav";
import { https } from "../../Services/config";
import dayjs from "dayjs";
import "./Header.css";
import { useDispatch, useSelector } from "react-redux";
import { setInfo } from "../../redux/headerSlice";
import { localServ } from "../../Services/localServ";
import { NavLink } from "react-router-dom";

const onChange = (key) => {
  console.log(key);
  if (key === "3") {
    window.location.href = "https://www.airbnb.com/s/experiences/online";
  }
};

const { Text, Title } = Typography;

const disabledDate = (current) => {
  return current && current < dayjs().endOf("day");
};

export default function HeaderMobile() {
  const [showButton, setShowButton] = useState(false);
  const [maViTri, setMaViTri] = useState(null);
  let dispatch = useDispatch();
  let headerInfo = useSelector((state) => state.headerSlice.headerInfo);
  const trimmedDiaDiem = headerInfo.diaDiem
    ? headerInfo.diaDiem.split(" - MVT:")[0]
    : null;
  const [totalAdult, setTotalAdult] = useState(1);
  const [totalChild, setTotalChild] = useState(0);

  const onNhanPhong = (value) => {
    if (value) {
      const formatDate = dayjs(value).format("YYYY/MM/DD");
      setSelectedInfo((prevInfo) => ({
        ...prevInfo,
        thoiGian: {
          ...prevInfo.thoiGian,
          nhanPhong: formatDate,
        },
      }));
    }
  };
  const onTraPhong = (value) => {
    if (value) {
      const formatDate = dayjs(value).format("YYYY/MM/DD");
      console.log("formatDate: ", formatDate);
      setSelectedInfo((prevInfo) => ({
        ...prevInfo,
        thoiGian: {
          ...prevInfo.thoiGian,
          traPhong: formatDate,
        },
      }));
    }
  };

  const [selectedInfo, setSelectedInfo] = useState({
    diaDiem: null,
    thoiGian: {
      nhanPhong: null,
      traPhong: null,
    },
    soKhach: null,
  });

  const handleSearch = () => {
    localServ.setInfo(selectedInfo);
    dispatch(setInfo(selectedInfo));
    console.log(selectedInfo);
  };

  const onChangeAdult = (value) => {
    setTotalAdult(value);
  };
  const onChangeChild = (value) => {
    setTotalChild(value);
  };

  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setSelectedInfo((prevSelectedInfo) => ({
      ...prevSelectedInfo,
      soKhach: totalAdult + totalChild,
    }));
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  const [scrolling, setScrolling] = useState(false);
  const [viTri, setVitri] = useState([]);
  useEffect(() => {
    const handleScroll = () => {
      const yOffset = window.pageYOffset;
      if (yOffset > 300) {
        setShowButton(true);
      } else {
        setShowButton(false);
      }
      if (window.scrollY > 0) {
        setScrolling(true);
      } else {
        setScrolling(false);
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    https
      .get("api/vi-tri")
      .then((res) => {
        setVitri(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
        message.error("Lấy thông tin thất bại!");
      });
  }, []);

  const options = viTri.map((item) => ({
    value: `${item.tenViTri} - MVT: ${item.id.toString()} `,
    label: item.tenViTri,
  }));

  const onSelect = (data) => {
    console.log(data);

    const startIndex = data.indexOf("MVT:") + 5;
    const numberString = data.slice(startIndex).trim();
    setMaViTri(parseInt(numberString));
    setSelectedInfo((prevSelectedInfo) => ({
      ...prevSelectedInfo,
      diaDiem: data,
    }));
  };

  return (
    <div className="pb-24">
      <button
        className={`${
          showButton ? "opacity-100" : "opacity-0"
        } transition-opacity duration-300 fixed bottom-10 right-10 bg-purple-800 text-white p-2 hover:bg-purple-900 rounded-full`}
        onClick={scrollToTop}
      >
        <ArrowUpOutlined className="w-5 h-5" />
      </button>
      <div className="shadow-md top-0 left-0 w-full fixed bg-white z-10">
        <div className="container mx-auto flex justify-between py-4">
          <div className="w-80">
            <img
              src={logo}
              alt="logo"
              width={120}
              className="cursor-pointer"
              onClick={() => {
                window.location.href = "/";
              }}
            />
          </div>
          <UserNav />
        </div>
      </div>
      <Modal
        title="Thêm khách"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <div className="flex justify-between p-3">
          <div className="">
            <Title level={3}>Người lớn</Title>
            <Text>Từ 18 tuổi trở lên</Text>
          </div>
          <InputNumber
            size="large"
            min={1}
            max={100}
            defaultValue={1}
            onChange={onChangeAdult}
          />
        </div>
        <div className="flex justify-between p-3">
          <div className="">
            <Title level={3}>Trẻ em</Title>
            <Text>Dưới 18 tuổi</Text>
          </div>
          <InputNumber
            size="large"
            min={0}
            max={100}
            defaultValue={0}
            onChange={onChangeChild}
          />
        </div>
      </Modal>
    </div>
  );
}
