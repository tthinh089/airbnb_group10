import React, { useEffect, useState } from "react";
import logo from "./logo.png";
import {
  SearchOutlined,
  UsergroupAddOutlined,
  ArrowUpOutlined,
} from "@ant-design/icons";
import {
  AutoComplete,
  DatePicker,
  Modal,
  Tabs,
  message,
  InputNumber,
  Typography,
} from "antd";
import UserNav from "./UserNav";
import { https } from "../../Services/config";
import dayjs from "dayjs";
import "./Header.css";
import { useDispatch, useSelector } from "react-redux";
import { setInfo } from "../../redux/headerSlice";
import { localServ } from "../../Services/localServ";
import { NavLink } from "react-router-dom";

const onChange = (key) => {
  console.log(key);
  if (key === "3") {
    window.location.href = "https://www.airbnb.com/s/experiences/online";
  }
};

const { Text, Title } = Typography;

const disabledDate = (current) => {
  return current && current < dayjs().endOf("day");
};

export default function HeaderTablet() {
  const [showButton, setShowButton] = useState(false);
  const [maViTri, setMaViTri] = useState(null);
  let dispatch = useDispatch();
  let headerInfo = useSelector((state) => state.headerSlice.headerInfo);
  const trimmedDiaDiem = headerInfo.diaDiem
    ? headerInfo.diaDiem.split(" - MVT:")[0]
    : null;
  const [totalAdult, setTotalAdult] = useState(1);
  const [totalChild, setTotalChild] = useState(0);

  const onNhanPhong = (value) => {
    if (value) {
      const formatDate = dayjs(value).format("YYYY/MM/DD");
      setSelectedInfo((prevInfo) => ({
        ...prevInfo,
        thoiGian: {
          ...prevInfo.thoiGian,
          nhanPhong: formatDate,
        },
      }));
    }
  };
  const onTraPhong = (value) => {
    if (value) {
      const formatDate = dayjs(value).format("YYYY/MM/DD");
      console.log("formatDate: ", formatDate);
      setSelectedInfo((prevInfo) => ({
        ...prevInfo,
        thoiGian: {
          ...prevInfo.thoiGian,
          traPhong: formatDate,
        },
      }));
    }
  };

  const [selectedInfo, setSelectedInfo] = useState({
    diaDiem: null,
    thoiGian: {
      nhanPhong: null,
      traPhong: null,
    },
    soKhach: null,
  });

  const handleSearch = () => {
    localServ.setInfo(selectedInfo);
    dispatch(setInfo(selectedInfo));
    console.log(selectedInfo);
  };

  const onChangeAdult = (value) => {
    setTotalAdult(value);
  };
  const onChangeChild = (value) => {
    setTotalChild(value);
  };

  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setSelectedInfo((prevSelectedInfo) => ({
      ...prevSelectedInfo,
      soKhach: totalAdult + totalChild,
    }));
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  const [scrolling, setScrolling] = useState(false);
  const [viTri, setVitri] = useState([]);
  useEffect(() => {
    const handleScroll = () => {
      const yOffset = window.pageYOffset;
      if (yOffset > 300) {
        setShowButton(true);
      } else {
        setShowButton(false);
      }
      if (window.scrollY > 0) {
        setScrolling(true);
      } else {
        setScrolling(false);
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    https
      .get("api/vi-tri")
      .then((res) => {
        setVitri(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
        message.error("Lấy thông tin thất bại!");
      });
  }, []);

  const options = viTri.map((item) => ({
    value: `${item.tenViTri} - MVT: ${item.id.toString()} `,
    label: item.tenViTri,
  }));

  const onSelect = (data) => {
    console.log(data);

    const startIndex = data.indexOf("MVT:") + 5;
    const numberString = data.slice(startIndex).trim();
    setMaViTri(parseInt(numberString));
    setSelectedInfo((prevSelectedInfo) => ({
      ...prevSelectedInfo,
      diaDiem: data,
    }));
  };

  const items = [
    {
      key: "1",
      label: "Nơi ở",
      children: (
        <div className="flex items-center justify-center gap-2 text-sm border-2 rounded-full">
          <div className="flex flex-col items-start hover:bg-gray-100 p-4 rounded-full w-1/3 cursor-pointer">
            <button className="font-bold h-full">Địa điểm</button>
            <AutoComplete
              style={{ width: 150 }}
              placeholder="Bạn sắp đi đâu?"
              options={options}
              filterOption={true}
              onSelect={onSelect}
            />
          </div>
          <span className="border border-gray-300 h-7"></span>
          <div className="flex flex-col items-start hover:bg-gray-100 p-4 rounded-full w-1/4 cursor-pointer">
            <button className="font-bold  h-full">Nhận phòng</button>
            <DatePicker
              format="YYYY/MM/DD"
              disabledDate={disabledDate}
              onChange={onNhanPhong}
            />
          </div>
          <span className="border border-gray-300 h-7"></span>
          <div className="flex flex-col items-start hover:bg-gray-100 p-4 rounded-full w-1/4 cursor-pointer">
            <button className="font-bold  h-full">Trả phòng</button>
            <DatePicker
              format="YYYY/MM/DD"
              disabledDate={disabledDate}
              onChange={onTraPhong}
            />
          </div>
          <span className="border border-gray-300 h-7"></span>
          <div className="flex items-start hover:bg-gray-100 p-4 rounded-full w-1/3 justify-between cursor-pointer">
            <div
              className="flex flex-col items-start hover:bg-gray-100 rounded-full"
              onClick={showModal}
            >
              <button className="font-bold  h-full">Khách</button>
              <span className="text-gray-400">
                {selectedInfo.soKhach === null ? (
                  <span className="text-gray-400">Thêm khách</span>
                ) : (
                  <span>{selectedInfo.soKhach} người</span>
                )}
              </span>
            </div>
            <NavLink
              to={`/diaDiem/${maViTri}`}
              onClick={handleSearch}
              className="flex justify-center items-center p-2 bg-red-500 hover:bg-red-600 rounded-full cursor-pointer text-2xl"
            >
              <SearchOutlined className="text-white" />
            </NavLink>
          </div>
        </div>
      ),
    },
    {
      key: "2",
      label: "Trải nghiệm",
      children: (
        <div className="flex items-center justify-center gap-2 text-sm border-2 rounded-full">
          <div className="flex flex-col items-start hover:bg-gray-100 p-4 rounded-full w-1/3 cursor-pointer">
            <button className="font-bold h-full">Địa điểm</button>
            <span className="text-gray-400">Bạn sắp đi đâu?</span>
          </div>
          <span className="border border-gray-300 h-7 "></span>
          <div className="flex flex-col items-start hover:bg-gray-100 p-4 rounded-full w-1/3 cursor-pointer">
            <button className="font-bold  h-full">Ngày</button>
            <span className="text-gray-400">Thêm ngày</span>
          </div>
          <span className="border border-gray-300 h-7"></span>
          <div className="flex items-start hover:bg-gray-100 p-4 rounded-full w-1/3 justify-between cursor-pointer">
            <div className="flex flex-col items-start hover:bg-gray-100 rounded-full">
              <button className="font-bold  h-full">Khách</button>
              <span className="text-gray-400">Thêm khách</span>
            </div>
            <span className="flex justify-center items-center p-2 bg-red-500 hover:bg-red-600 rounded-full cursor-pointer text-2xl">
              <SearchOutlined className="text-white" />
            </span>
          </div>
        </div>
      ),
    },
    {
      key: "3",
      label: "Trải nghiệm trực tuyến",
    },
  ];

  return (
    <div className="pb-24">
      <button
        className={`${
          showButton ? "opacity-100" : "opacity-0"
        } transition-opacity duration-300 fixed bottom-10 right-10 bg-purple-800 text-white p-2 hover:bg-purple-900 rounded-full`}
        onClick={scrollToTop}
      >
        <ArrowUpOutlined className="w-5 h-5" />
      </button>
      <div className="shadow-md top-0 left-0 w-full fixed bg-white z-10">
        <div className="container mx-auto flex justify-between py-4">
          <div className="w-80">
            <img
              src={logo}
              alt="logo"
              width={120}
              className="cursor-pointer"
              onClick={() => {
                window.location.href = "/";
              }}
            />
          </div>
          <UserNav />
        </div>
        <div className="flex-grow my-auto" onClick={scrollToTop}>
          {scrolling ? (
            <div
              className={`flex items-center space-x-3 justify-around px-4 py-2 bg-white border-2 rounded-full text-sm shadow-md transition-all duration-300 ${
                scrolling ? "h-auto opacity-100" : "h-0 opacity-0"
              }`}
            >
              <button className="font-bold h-full py-3">
                {trimmedDiaDiem || "Anywhere"}
              </button>
              <span className="border border-gray-300 h-5"></span>{" "}
              <button className="font-bold h-full">
                {headerInfo.thoiGian &&
                headerInfo.thoiGian.nhanPhong !== null &&
                headerInfo.thoiGian.traPhong !== null
                  ? `Từ ${headerInfo.thoiGian.nhanPhong} - Đến ${headerInfo.thoiGian.traPhong}`
                  : "Any week"}
              </button>
              <span className="border border-gray-300 h-5"></span>{" "}
              <button className="text-gray-600 h-full">
                {headerInfo.soKhach
                  ? `${headerInfo.soKhach} khách`
                  : "Add guests"}
              </button>
              <span className="flex justify-center items-center p-2 bg-red-500 rounded-full cursor-pointer">
                <UsergroupAddOutlined className="text-white" />
              </span>
            </div>
          ) : (
            <div className="container mx-auto pb-2 flex-grow">
              <div
                className={`transition-all duration-300 ${
                  scrolling ? "h-0 opacity-0" : "h-auto opacity-100"
                }`}
              >
                <Tabs
                  defaultActiveKey="1"
                  centered
                  items={items}
                  onChange={onChange}
                />
              </div>
            </div>
          )}
        </div>
      </div>
      <Modal
        title="Thêm khách"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <div className="flex justify-between p-3">
          <div className="">
            <Title level={3}>Người lớn</Title>
            <Text>Từ 18 tuổi trở lên</Text>
          </div>
          <InputNumber
            size="large"
            min={1}
            max={100}
            defaultValue={1}
            onChange={onChangeAdult}
          />
        </div>
        <div className="flex justify-between p-3">
          <div className="">
            <Title level={3}>Trẻ em</Title>
            <Text>Dưới 18 tuổi</Text>
          </div>
          <InputNumber
            size="large"
            min={0}
            max={100}
            defaultValue={0}
            onChange={onChangeChild}
          />
        </div>
      </Modal>
    </div>
  );
}
