import React from "react";
import { FaUserAstronaut, FaPlaceOfWorship, FaDumpster } from "react-icons/fa";
import { NavLink } from "react-router-dom";
export default function Menu() {
  const classActive =
    "relative px-4 py-3 flex items-center space-x-4 rounded-lg text-white bg-gradient-to-r from-sky-600 to-cyan-400";
  const classNonActive =
    "px-4 py-3 flex items-center space-x-4 rounded-md text-gray-500 group  hover:bg-purple-300";
  return (
    <div className="z-10 bg-white w-64 h-screen fixed rounded-none border-none">
      <div className="p-4 space-y-4">
        <NavLink
          to={"/admin/user-management"}
          aria-label="dashboard"
          className={
            window.location.pathname === "/admin/user-management"
              ? classActive
              : classNonActive
          }
        >
          <FaUserAstronaut />
          <span className="-mr-1 font-medium">Quản lý người dùng</span>
        </NavLink>
        <NavLink
          to={"/admin/location-management"}
          className={
            window.location.pathname === "/admin/location-management"
              ? classActive
              : classNonActive
          }
        >
          <FaPlaceOfWorship />
          <span>Quản lý thông tin vị trí</span>
        </NavLink>
        <NavLink
          to={"/admin/room-management"}
          className={
            window.location.pathname === "/admin/room-management"
              ? classActive
              : classNonActive
          }
        >
          <FaDumpster />
          <span>Quản Lý thông tin phòng</span>
        </NavLink>
        <NavLink
          to={"/admin/reservation-management"}
          className={
            window.location.pathname === "/admin/reservation-management"
              ? classActive
              : classNonActive
          }
        >
          <FaDumpster />
          <span>Quản Lý thông tin đặt phòng </span>
        </NavLink>
      </div>
    </div>
  );
}
