import React, { useEffect, useState } from "react";
import { FaArrowCircleUp } from "react-icons/fa";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function HeaderAdmin() {


  return (
    <div  style={{backgroundColor:'#ACB6E5'}}> 
      <UserNav />
    </div>
  );
}
