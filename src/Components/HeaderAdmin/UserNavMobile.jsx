import { message } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import logo from "../../assets/image/logo_airbnb.png";
import { useNavigate } from "react-router";
import style from "../../assets/style/style.css";
import { localServ } from "../../Services/localServ";
import { FaAlignJustify, FaBell, FaHouseUser } from "react-icons/fa";
import "flowbite";
import { NavLink } from "react-router-dom";
import Menu from "./Menu";
export default function UserNavMobile() {
  const [isLogin, setIsLogin] = useState(false);
  const navigate = useNavigate();
  const { userInfo } = useSelector((state) => state.userSlice);
  const user = userInfo;
  const [isShowNav, setIsShowNav] = useState(false);
  const [isShowInfo, setIsShowInfo] = useState(false);
  useEffect(() => {
    if (user && user.role === "ADMIN") {
      setIsLogin(true);
    } else {
      setIsLogin(false);
      message.error("Bạn không có quyền truy cập vào trang này");
      navigate("/");
    }
  }, [user]);

  return (
    <>
      {isLogin ? (
        <div>
          <nav className="bg-white border-b border-gray-300">
            <div className="flex justify-between items-center px-5">
              <button
                onClick={() => {
                  setIsShowNav(!isShowNav);
                }}
              >
                <FaAlignJustify className="text-cyan-500 text-xl" />
              </button>

              <NavLink className="ml-1" to={"/"}>
                <img
                  src={`${logo}`}
                  alt="logo"
                  className="h-12 w-18 object-fill object-center"
                />
              </NavLink>

              <div className="space-x-0">
                <button>
                  <FaBell className="text-cyan-500 text-xl" />
                </button>

                <button
                  onClick={() => {
                    setIsShowInfo(!isShowInfo);
                  }}
                  className="text-white focus:ring-4  focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-xl px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  type="button"
                >
                  {user.avatar ? (
                    <img
                      src={`${user.avatar}`}
                      alt="admin An danh "
                      className="h-6 w-8 rounded-md object-cover"
                    />
                  ) : (
                    <FaHouseUser className="text-cyan-500 text-2xl" />
                  )}
                </button>
                {/* Dropdown menu   */}
                {isShowInfo ? (
                  <div className="z-10 absolute right-0 top-20  bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700">
                    <ul
                      className="py-2 text-sm text-gray-700 dark:text-gray-200"
                      aria-labelledby="dropdownDelayButton"
                    >
                      <li>
                        <NavLink
                          to={"/account"}
                          className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                        >
                          Hello Admin
                          <span className="text-red-400 cursor-pointer underline ml-1">
                            {user.name}
                          </span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink
                          className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                          onClick={() => {
                            setTimeout(() => {
                              setIsLogin(false);
                              navigate("/");
                              localServ.removeUser();
                            }, 1000);
                          }}
                        >
                          Sign out
                        </NavLink>
                      </li>
                      <li>
                        <NavLink className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
                          Settings
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </nav>
          {/* Menu */}
          {isShowNav ? <Menu /> : ""}
        </div>
      ) : (
        <></>
      )}
    </>
  );
}
