import React from 'react'
import { Desktop, Mobile, Tablet } from '../../responsives/responsive'
import UserNavDesktop from './UserNavDesktop'
import UserNavTablet from './UserNavTablet'
import UserNavMobile from './UserNavMobile'

export default function UserNav() {
  return (
      <>
          <Desktop>
              <UserNavDesktop/>
          </Desktop>
          <Tablet>
              <UserNavTablet/>
          </Tablet>
          <Mobile>
              <UserNavMobile/>
          </Mobile>
      </>
  )
}
