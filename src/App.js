import "./App.css";
import { Navigate, Route, Routes } from "react-router-dom";
import HomeLayout from "./Layout/HomeLayout";
import HomePage from "./Pages/Home/HomePage";
import RegisterPage from "./Pages/Register/RegisterPage";
import AdminLayout from "./Layout/AdminLayout";
import UserManagementPage from "./Pages/Admin/UserManagement/UserManagementPage";
import LocationManagementPage from "./Pages/Admin/LocationManagement/LocationManagementPage";
import RoomManagement from "./Pages/Admin/RoomManagement/RoomManagement";
import BookingManagement from "./Pages/Admin/BookingManagement/BookingManagement";
import DanhSachPhong from "./Pages/DanhSachPhong/DanhSachPhong";
import Login from "./Pages/Login/Login";
import ChiTietPhong from "./Pages/ChiTietPhong/ChiTietPhong";
import Page404 from "./Pages/Page404/Page404";
import AccountPage from "./Pages/AccountPage/AccountPage";

function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<HomeLayout contentPage={<HomePage />} />} />
        <Route path="/register" element={<RegisterPage />} />
        {/* ADMIN  */}
        <Route path="/admin">
          <Route index element={<Navigate to="user-management" />} />
          <Route
            path="user-management"
            element={<AdminLayout contentPage={<UserManagementPage />} />}
          />
          <Route
            path="location-management"
            element={<AdminLayout contentPage={<LocationManagementPage />} />}
          />
          <Route
            path="room-management"
            element={<AdminLayout contentPage={<RoomManagement />} />}
          />
          <Route
            path="reservation-management"
            element={<AdminLayout contentPage={<BookingManagement />} />}
          />
        </Route>
        <Route path="/account" element={<AccountPage />} />
        <Route path="/diaDiem/:cityID" element={<DanhSachPhong />} />
        <Route path="/diaDiem/:cityID/:roomID" element={<ChiTietPhong />} />
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<Page404 />} />
      </Routes>
    </div>
  );
}

export default App;
