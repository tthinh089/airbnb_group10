import { createSlice } from "@reduxjs/toolkit";
import { localServ } from "../Services/localServ";

const headerInfo = localServ.getInfo();

const initialState = {
  headerInfo: headerInfo === null ? [] : headerInfo,
};

const headerSlice = createSlice({
  name: "headerSlice",
  initialState,
  reducers: {
    setInfo: (state, action) => {
      state.headerInfo = action.payload;
    },
  },
});

export const { setInfo } = headerSlice.actions;

export default headerSlice.reducer;
