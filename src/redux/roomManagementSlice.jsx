import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listRoom: [],
  roomSelected: {},
  mess: true,
  togglePopup: false,
  togglePopupDetail: false,
  listRoomSearch: [],
  togglePopupUpload: false,
  localSelect: "",
};

const roomManagementSlice = createSlice({
  name: "roomManagementSlice",
  initialState,
  reducers: {
    setRoomSelected: (state, action) => {
      state.roomSelected = action.payload;
    },
    setListRoom: (state, action) => {
      state.listRoom = action.payload;
    },
    setMess: (state, action) => {
      state.mess = action.payload;
    },
    setTogglePopup: (state, action) => {
      state.togglePopup = action.payload;
    },
    setTogglePopupDetail: (state, action) => {
      state.togglePopupDetail = action.payload;
    },
    setlistRoomSearch: (state, action) => {
      state.listRoomSearch = action.payload;
    },
    setTogglePopupUpload: (state, action) => {
      state.togglePopupUpload = action.payload;
    },
     setLocalSelect: (state, action) => {
      state.localSelect = action.payload;
    }
  },
});

export const {
  setRoomSelected,
  setListRoom,
  setMess,
  setTogglePopup,
  setTogglePopupDetail,
  setlistRoomSearch,
  setTogglePopupUpload,
  setLocalSelect
} = roomManagementSlice.actions;

export default roomManagementSlice.reducer;
