import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listBooking: [],
  bookingSelected: {},
  mess: true,
  togglePopup: false,
  togglePopupDetail: false,
};
const bookingManagementSlice = createSlice({
  name: "bookingManagementSlice",
  initialState,
  reducers: {
    setListBooking: (state, action) => {
      state.listBooking = action.payload;
    },
    setBookingSelected: (state, action) => {
      state.bookingSelected = action.payload;
    },
    setMess: (state, action) => {
      state.mess = action.payload;
    },
    setTogglePopup: (state, action) => {
      state.togglePopup = action.payload;
    },
    setTogglePopupDetail: (state, action) => {
      state.togglePopupDetail = action.payload;
    },
  },
});

export const {setListBooking,setBookingSelected,setMess, setTogglePopup, setTogglePopupDetail} = bookingManagementSlice.actions;

export default bookingManagementSlice.reducer;
