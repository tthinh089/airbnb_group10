import { createSlice } from "@reduxjs/toolkit";
import { localServ } from "../Services/localServ";

const initialState = {
  userInfo: localServ.getUser() || "",
  tokenUser: localServ.getTokenUser() || "",
};
const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setLogin: (state, action) => {
      state.userInfo = action.payload;
    },
    setTokenUser: (state, action) => {
      state.tokenUser = action.payload;
    },
    updateImageUrl: (state, action) => {
      state.userInfo.avatar = action.payload;
    },
  },
});

export const { setLogin, updateImageUrl, setTokenUser } = userSlice.actions;

export default userSlice.reducer;
