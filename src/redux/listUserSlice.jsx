import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    listUser: [],
    uerSelected: {},
    mess: true,
    togglePopup: false,
    togglePopupDetail: false,
    listUserSearch: [], 
}

const listUserSlice = createSlice({
  name: "listUserSlice",
  initialState,
    reducers: {
        setUserSelected: (state, action) => { 
            state.uerSelected = action.payload;
        },
        setListUser: (state, action) => { 
            state.listUser = action.payload;
        },
        setMess: (state, action) => { 
            state.mess = action.payload; 
        },
        setTogglePopup: (state,action) => { 
            state.togglePopup = action.payload;
        },
        setTogglePopupDetail: (state, action) => { 
            state.togglePopupDetail = action.payload;
        },
        setlistUserSearch: (state, action) => { 
            state.listUserSearch = action.payload;      }
  }
});

export const {setUserSelected, setListUser, setMess, setTogglePopup,setTogglePopupDetail, setlistUserSearch} = listUserSlice.actions

export default listUserSlice.reducer