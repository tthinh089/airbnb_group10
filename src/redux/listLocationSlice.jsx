import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listLocation: [],
  locationSelected: {},
  mess: true,
  togglePopup: false,
  togglePopupDetail: false,
  togglePopupUpload: false,
};
const listLocationSlice = createSlice({
  name: "listLocationSlice",
  initialState,
  reducers: {
    setListLocation: (state, action) => {
      state.listLocation = action.payload;
    },
    setLocationSelected: (state, action) => {
      state.locationSelected = action.payload;
    },
    setMess: (state, action) => {
      state.mess = action.payload;
    },
    setTogglePopup: (state, action) => {
      state.togglePopup = action.payload;
    },
    setTogglePopupDetail: (state, action) => {
      state.togglePopupDetail = action.payload;
    },
    setTogglePopupUpload: (state, action) => { 
      state.togglePopupUpload = action.payload;
     }
  },
});

export const {setListLocation,setLocationSelected,setMess, setTogglePopup, setTogglePopupDetail, setlistLocationSearch, setTogglePopupUpload} = listLocationSlice.actions;

export default listLocationSlice.reducer;
